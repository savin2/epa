package ro.proiect.epa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.proiect.epa.model.AnimalDetails;
import ro.proiect.epa.repository.AnimalDetailsRepository;

import java.util.Optional;

@Service
@Transactional
public class AnimalDetailsServiceImpl implements AnimalDetailsService {

    private AnimalDetailsRepository animalDetailsRepository;

    @Autowired
    public AnimalDetailsServiceImpl(AnimalDetailsRepository animalDetailsRepository) {
        this.animalDetailsRepository = animalDetailsRepository; }


    @Override
    public AnimalDetails createAnimalDetails(AnimalDetails animalDetails) {
        return animalDetailsRepository.save(animalDetails);
    }


    @Override
    public AnimalDetails getAnimalDetailsBySex (AnimalDetails.Sex sex) {
        return animalDetailsRepository.findBySex(sex);
    }

    @Override
    public AnimalDetails findById(Long theId) {
        Optional<AnimalDetails> result = animalDetailsRepository.findById(theId);

        AnimalDetails animalDetails = null;

        if (result.isPresent()) {
            animalDetails = result.get();
        }
        else {
            // we didn't find the animal
            throw new RuntimeException("Did not find animal id - " + theId);
        }

        return animalDetails;
    }

    @Override
    public void deleteById(Long id) {
        animalDetailsRepository.deleteById(id);
    }
}
