package ro.proiect.epa.service;

import ro.proiect.epa.model.AnimalDetails;
import ro.proiect.epa.model.AnimalType;

import java.util.List;

public interface AnimalDetailsService {

    AnimalDetails createAnimalDetails(AnimalDetails animalDetails);

    AnimalDetails findById(Long theId);

    AnimalDetails getAnimalDetailsBySex(AnimalDetails.Sex sex);

    void deleteById(Long id);
}
