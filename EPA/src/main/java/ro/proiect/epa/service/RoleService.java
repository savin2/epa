package ro.proiect.epa.service;

import ro.proiect.epa.model.Role;

import java.util.List;

public interface RoleService {
    Role createRole(Role role);

    List<Role> findAll();
}
