package ro.proiect.epa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.AnimalMedicalHistory;
import ro.proiect.epa.model.AnimalType;
import ro.proiect.epa.model.User;
import ro.proiect.epa.repository.AnimalRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AnimalServiceImpl implements AnimalService {
    private AnimalRepository animalRepository;

    @Autowired
    public AnimalServiceImpl(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    @Override
    public void createAnimal(Animal animal) { animalRepository.save(animal); }

    @Override
    public void updateAnimal(Long id, Animal updatedAnimal) {
        Animal animal = animalRepository.getOne(id);
        animal.setTitle(updatedAnimal.getTitle());
        animal.setUrl(updatedAnimal.getUrl());
        animalRepository.save(animal);

    }

    @Override
    public void deleteAnimal(Long id) { animalRepository.deleteById(id); }

    @Override
    public List<Animal> findAll() { return animalRepository.findAll(); }

    @Override
    public List<Animal> findByUserAdded(User user) { return animalRepository.findByUserAdded(user); }

    @Override
    public List<Animal> findByAnimalType(AnimalType animalType) {
        return animalRepository.findByAnimalType(animalType);
    }

    @Override
    public Animal findById(Long theId) {
        Optional<Animal> result = animalRepository.findById(theId);

        Animal animal = null;

        if (result.isPresent()) {
            animal = result.get();
        }
        else {
            // we didn't find the employee
            throw new RuntimeException("Did not find employee id - " + theId);
        }

        return animal;
    }

    @Override
    public void deleteById(Long id) {
        animalRepository.deleteById(id);
    }
}
