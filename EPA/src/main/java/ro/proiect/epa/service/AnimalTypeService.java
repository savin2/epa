package ro.proiect.epa.service;

import ro.proiect.epa.model.AnimalMedicalHistory;
import ro.proiect.epa.model.AnimalType;

import java.util.List;

public interface AnimalTypeService {

    AnimalType createAnimalType(AnimalType animalType);

    List<AnimalType> findAll();

    AnimalType findById(Long theId);

    AnimalType getAnimalTypeByName(String name);

    void deleteById(Long id);
}
