package ro.proiect.epa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.proiect.epa.model.AdoptionRequest;
import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.User;
import ro.proiect.epa.repository.AdoptionRequestRepository;
import ro.proiect.epa.repository.AnimalRepository;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class AdoptionRequestServiceImpl implements AdoptionRequestService {

    @Autowired
    private AdoptionRequestRepository adoptionRequestRepository;
    @Autowired
    private AnimalRepository animalRepository;

    @Override
    public AdoptionRequest create(Long animalId, User user) {

        Animal animal = animalRepository.findById(animalId).get();

        AdoptionRequest request = new AdoptionRequest();
        request.setAnimal(animal);
        request.setUserRequested(user);
        request.setDate_created(new Date());
        request.setStatus(AdoptionRequest.Status.Requested);

        request = adoptionRequestRepository.save(request);
        return request;
    }

    @Override
    public boolean canRequest(Long animalId, User user) {
        boolean existing = adoptionRequestRepository
                .findByUserRequested(user).stream().anyMatch(ar -> ar.getAnimal().getId().equals(animalId));

        return !existing;
    }

    @Override
    public List<AdoptionRequest> findAll() { return adoptionRequestRepository.findAll(); }

    @Override
    public List<AdoptionRequest> findByUser(User user) { return adoptionRequestRepository.findByUserRequested(user); }

    @Override
    public void approve(Long id, User user){
        AdoptionRequest request = adoptionRequestRepository.findById(id).get();
        request.setStatus(AdoptionRequest.Status.Approved);
        request.setDate_closed(new Date());
        request.setUserAdmin(user);
        adoptionRequestRepository.save(request);
    }

    @Override
    public void decline(Long id, User user){
        AdoptionRequest request = adoptionRequestRepository.findById(id).get();
        request.setStatus(AdoptionRequest.Status.Declined);
        request.setDate_closed(new Date());
        request.setUserAdmin(user);
        adoptionRequestRepository.save(request);
    }

}
