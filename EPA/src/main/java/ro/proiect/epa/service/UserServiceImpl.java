package ro.proiect.epa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.proiect.epa.commands.ChangePasswordCommand;
import ro.proiect.epa.model.Role;
import ro.proiect.epa.model.User;
import ro.proiect.epa.repository.RoleRepository;
import ro.proiect.epa.repository.UserRepository;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public User createUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByName("USER");
        user.setRoles(new ArrayList<>(Collections.singletonList(userRole)));
        return userRepository.save(user);
    }

    @Override
    public User changeRoleToAdmin(User user) {
        Role adminRole = roleRepository.findByName("ADMIN");
        user.setRoles(new ArrayList<>(Collections.singletonList(adminRole)));
        return userRepository.save(user);
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isUserEmailPresent(String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    public boolean isUserNamePresent(String username) {
        return userRepository.findByUsername(username) != null;
    }

    @Override
    public boolean isOldPasswordWrong(ChangePasswordCommand changePasswordCommand) {
        User user = userRepository.findById(changePasswordCommand.getId()).orElse(null);

        if(bCryptPasswordEncoder.matches(changePasswordCommand.getOldPassword(), user.getPassword())) {
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User changePassword(ChangePasswordCommand changePasswordCommand) {
        User user = userRepository.findById(changePasswordCommand.getId()).orElse(null);

        user.setPassword(bCryptPasswordEncoder.encode(changePasswordCommand.getNewPassword()));

        return userRepository.save(user);
    }

}
