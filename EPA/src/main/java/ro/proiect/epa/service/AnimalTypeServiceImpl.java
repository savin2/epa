package ro.proiect.epa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.proiect.epa.model.AnimalType;
import ro.proiect.epa.repository.AnimalTypeRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AnimalTypeServiceImpl implements AnimalTypeService {

    private AnimalTypeRepository animalTypeRepository;

    @Autowired
    public AnimalTypeServiceImpl(AnimalTypeRepository animalTypeRepository) { this.animalTypeRepository = animalTypeRepository; }


    @Override
    public AnimalType createAnimalType(AnimalType animalType) {
        return animalTypeRepository.save(animalType);
    }

    @Override
    public List<AnimalType> findAll() { return animalTypeRepository.findAll();
    }

    @Override
    public AnimalType getAnimalTypeByName(String name) {
        return animalTypeRepository.findByName(name);
    }

    @Override
    public AnimalType findById(Long theId) {
        Optional<AnimalType> result = animalTypeRepository.findById(theId);

        AnimalType animalType = null;

        if (result.isPresent()) {
            animalType = result.get();
        }
        else {
            // we didn't find the employee
            throw new RuntimeException("Did not find employee id - " + theId);
        }

        return animalType;
    }

    @Override
    public void deleteById(Long id) {
        animalTypeRepository.deleteById(id);
    }
}
