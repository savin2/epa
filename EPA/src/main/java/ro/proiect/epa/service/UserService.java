package ro.proiect.epa.service;


import ro.proiect.epa.commands.ChangePasswordCommand;
import ro.proiect.epa.model.User;

import java.util.List;

public interface UserService {
    User createUser(User user);

    User changeRoleToAdmin(User user);

    User getUserByEmail(String email);

    boolean isUserEmailPresent(String email);

    boolean isUserNamePresent(String username);

    boolean isOldPasswordWrong(ChangePasswordCommand changePasswordCommand);

    User getUserById(Long userId);

    User changePassword(ChangePasswordCommand changePasswordCommand);
}
