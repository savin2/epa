package ro.proiect.epa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.proiect.epa.model.User;
import ro.proiect.epa.model.UserProfile;
import ro.proiect.epa.repository.UserProfileRepository;

import java.util.Optional;

@Service
@Transactional
public class UserProfileServiceImpl implements UserProfileService {

    private UserProfileRepository userProfileRepository;

    @Autowired
    public UserProfileServiceImpl(UserProfileRepository userProfileRepository) { this.userProfileRepository = userProfileRepository; }


    @Override
    public UserProfile createUserProfile(UserProfile userProfile) {
        return userProfileRepository.save(userProfile);
    }

    @Override
    public UserProfile findByUser(User user) { return userProfileRepository.findByUser(user); }

    @Override
    public UserProfile findById(Long theId) {
        Optional<UserProfile> result = userProfileRepository.findById(theId);

        UserProfile userProfile = null;

        if (result.isPresent()) {
            userProfile = result.get();
        }
        else {
            // we didn't find the employee
            throw new RuntimeException("Did not find employee id - " + theId);
        }

        return userProfile;
    }

    @Override
    public void deleteById(Long id) {
        userProfileRepository.deleteById(id);
    }
}
