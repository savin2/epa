package ro.proiect.epa.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.proiect.epa.model.AnimalMedicalHistory;
import ro.proiect.epa.repository.AnimalMedicalHistoryRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AnimalMedicalHistoryServiceImpl implements AnimalMedicalHistoryService {

    private AnimalMedicalHistoryRepository animalMedicalHistoryRepository;

    @Autowired
    public AnimalMedicalHistoryServiceImpl(AnimalMedicalHistoryRepository animalMedicalHistoryRepository) {
        this.animalMedicalHistoryRepository = animalMedicalHistoryRepository; }


    @Override
    public AnimalMedicalHistory createAnimalMedicalHistory(AnimalMedicalHistory animalMedicalHistory) {
        return animalMedicalHistoryRepository.save(animalMedicalHistory);
    }


    @Override
    public List<AnimalMedicalHistory> getAnimalMedicalHistoryByNeutering (AnimalMedicalHistory.Neutering neutering) {
        return animalMedicalHistoryRepository.findByNeutering(neutering);
    }

    @Override
    public AnimalMedicalHistory findById(Long theId) {
        Optional<AnimalMedicalHistory> result = animalMedicalHistoryRepository.findById(theId);

        AnimalMedicalHistory animalMedicalHistory = null;

        if (result.isPresent()) {
            animalMedicalHistory = result.get();
        }
        else {
            // we didn't find the employee
            throw new RuntimeException("Did not find employee id - " + theId);
        }

        return animalMedicalHistory;
    }

    @Override
    public void deleteById(Long id) {
        animalMedicalHistoryRepository.deleteById(id);
    }
}
