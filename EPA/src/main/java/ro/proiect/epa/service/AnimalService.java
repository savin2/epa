package ro.proiect.epa.service;

import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.AnimalMedicalHistory;
import ro.proiect.epa.model.AnimalType;
import ro.proiect.epa.model.User;

import java.util.List;

public interface AnimalService {

    void createAnimal(Animal animal);

    void updateAnimal(Long id, Animal animal);

    void deleteAnimal(Long id);

    List<Animal> findAll();

    List<Animal> findByAnimalType(AnimalType animalType);

    List<Animal> findByUserAdded(User user);

    Animal findById(Long theId);

    void deleteById(Long id);
}
