package ro.proiect.epa.service;

import ro.proiect.epa.model.AnimalMedicalHistory;

import java.util.List;

public interface AnimalMedicalHistoryService {

    AnimalMedicalHistory createAnimalMedicalHistory(AnimalMedicalHistory animalMedicalHistory);

     AnimalMedicalHistory findById(Long theId);

    List<AnimalMedicalHistory> getAnimalMedicalHistoryByNeutering(AnimalMedicalHistory.Neutering neutering);

    void deleteById(Long id);
}
