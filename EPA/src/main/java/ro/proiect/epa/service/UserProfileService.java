package ro.proiect.epa.service;

import ro.proiect.epa.model.User;
import ro.proiect.epa.model.UserProfile;

public interface UserProfileService {

    UserProfile createUserProfile(UserProfile userProfile);

    UserProfile findById(Long theId);

    UserProfile findByUser(User user);

    void deleteById(Long id);
}
