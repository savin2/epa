package ro.proiect.epa.service;

import ro.proiect.epa.model.AdoptionRequest;
import ro.proiect.epa.model.User;

import java.util.List;

public interface AdoptionRequestService {
    AdoptionRequest create(Long animalId, User user);
    boolean canRequest(Long animalId, User user);
    List<AdoptionRequest> findAll();
    List<AdoptionRequest> findByUser(User user);
    void approve(Long id, User user);
    void decline(Long id, User user);
}
