package ro.proiect.epa.dataloader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ro.proiect.epa.model.*;
import ro.proiect.epa.repository.AdoptionRequestRepository;
import ro.proiect.epa.service.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Component
@Transactional
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private UserService userService;
    private RoleService roleService;
    private AnimalService animalService;
    private AnimalTypeService animalTypeService;
    private UserProfileService userProfileService;
    private AnimalMedicalHistoryService animalMedicalHistoryService;
    private AnimalDetailsService animalDetailsService;
    private AdoptionRequestRepository adoptionRequestRepository;

    @Autowired
    public InitialDataLoader(UserService userService, RoleService roleService,
                             AnimalService animalService, AnimalTypeService animalTypeService,
                             UserProfileService userProfileService, AdoptionRequestRepository adoptionRequestRepository, AnimalMedicalHistoryService animalMedicalHistoryService, AnimalDetailsService animalDetailsService) {
        this.userService = userService;
        this.roleService = roleService;
        this.animalService = animalService;
        this.animalTypeService = animalTypeService;
        this.userProfileService = userProfileService;
        this.animalMedicalHistoryService = animalMedicalHistoryService;
        this.animalDetailsService = animalDetailsService;
        this.adoptionRequestRepository = adoptionRequestRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        //ROLES
        roleService.createRole(new Role("ADMIN"));
        roleService.createRole(new Role("USER"));

        //USERS

        User user_admin = new User("admin", "admin@admin.com", "admin@admin.com");
        userService.createUser(user_admin);
        userService.changeRoleToAdmin(user_admin);

        User user_normal = new User("normal", "normal@normal.com", "normal@normal.com");
        userService.createUser(user_normal);

        //ANIMAL TYPES

        animalTypeService.createAnimalType(new AnimalType("Caine"));
        animalTypeService.createAnimalType(new AnimalType("Pisica"));
        animalTypeService.createAnimalType(new AnimalType("Hamster"));
        animalTypeService.createAnimalType(new AnimalType("Peste"));
        animalTypeService.createAnimalType(new AnimalType("Alpaca"));
        animalTypeService.createAnimalType(new AnimalType("Papagal"));

        //Animal

        Animal animal1 = new Animal();
        animal1.setTitle("Caine bland in doua culori");
        animal1.setUrl("https://www.animalzoo.ro/wp-content/uploads/2017/05/cu11-604x515.jpg");
        animal1.setAnimalType(animalTypeService.getAnimalTypeByName("Caine"));

        Animal animal2 = new Animal();
        animal2.setTitle("Alpaca care nu mananca mobila");
        animal2.setUrl("https://i.pinimg.com/236x/5f/e4/5c/5fe45c1bd0162cc1a40e096ae1d33831--happy-smile-happy-baby.jpg");
        animal2.setAnimalType(animalTypeService.getAnimalTypeByName("Alpaca"));


        Animal animal3 = new Animal();
        animal3.setTitle("Melanotaenia trifasciata");
        animal3.setUrl("https://www.nevertebrate.ro/images/M.trifasciata%20[Rocky%20Bottom%20Creek,%20Goyder%20R]%20GS.jpg");
        animal3.setAnimalType(animalTypeService.getAnimalTypeByName("Peste"));


        animalService.createAnimal(animal1);
        animalService.createAnimal(animal2);
        animalService.createAnimal(animal3);


        AnimalMedicalHistory animalMedicalHistory1 = new AnimalMedicalHistory();
        animalMedicalHistory1.setKnown_disease("Parvoviroza");
        try {
            animalMedicalHistory1.setDate_from_last_vaccine((new SimpleDateFormat("dd/MM/yyyy").parse("12/12/2019")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        animalMedicalHistory1.setNeutering(AnimalMedicalHistory.Neutering.Yes);

        AnimalMedicalHistory animalMedicalHistory2 = new AnimalMedicalHistory();
        animalMedicalHistory2.setKnown_disease("Maladia Carre");
        animalMedicalHistory2.setNeutering(AnimalMedicalHistory.Neutering.No);
        try {
            animalMedicalHistory2.setDate_from_last_vaccine((new SimpleDateFormat("dd/MM/yyyy").parse("12/07/2018")));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        AnimalMedicalHistory animalMedicalHistory3 = new AnimalMedicalHistory();
        animalMedicalHistory3.setKnown_disease("Coronavirus");
        animalMedicalHistory3.setNeutering(AnimalMedicalHistory.Neutering.Yes);

        try {
            animalMedicalHistory3.setDate_from_last_vaccine((new SimpleDateFormat("dd/MM/yyyy").parse("19/03/2020")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        animal1.setAnimalMedicalHistory(animalMedicalHistory1);
        animal2.setAnimalMedicalHistory(animalMedicalHistory2);
        animal3.setAnimalMedicalHistory(animalMedicalHistory3);


        AnimalDetails animalDetails1 = new AnimalDetails();
        animalDetails1.setDescription("It has a disease");
        animalDetails1.setSex(AnimalDetails.Sex.Male);
        try {
            animalDetails1.setArrival_date((new SimpleDateFormat("dd/MM/yyyy").parse("11/12/2019")));
            animalDetails1.setEstimated_birth_date((new SimpleDateFormat("dd/MM/yyyy").parse("08/09/2017")));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        AnimalDetails animalDetails2 = new AnimalDetails();
        animalDetails2.setDescription("It's healthy");
        animalDetails2.setSex(AnimalDetails.Sex.Female);
        try {
            animalDetails2.setArrival_date((new SimpleDateFormat("dd/MM/yyyy").parse("11/05/2018")));
            animalDetails2.setEstimated_birth_date((new SimpleDateFormat("dd/MM/yyyy").parse("02/02/2016")));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        AnimalDetails animalDetails3 = new AnimalDetails();
        animalDetails3.setDescription("It has a disease");
        animalDetails3.setSex(AnimalDetails.Sex.Male);
        try {
            animalDetails3.setArrival_date((new SimpleDateFormat("dd/MM/yyyy").parse("19/03/2020")));
            animalDetails3.setEstimated_birth_date((new SimpleDateFormat("dd/MM/yyyy").parse("02/02/2014")));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        animal1.setAnimalDetails(animalDetails1);
        animal2.setAnimalDetails(animalDetails2);
        animal3.setAnimalDetails(animalDetails3);

        //USER PROFILES

        UserProfile up1 = new UserProfile();
        up1.setFirstName("Cornel");
        up1.setLastName("Popescu");
        up1.setPhoneNumber("0723232323");
        up1.setCountry("Romania");
        up1.setUser(user_admin);
        userProfileService.createUserProfile(up1);

        //Adoption Request
        AdoptionRequest request = new AdoptionRequest();
        request.setAnimal(animal1);
        request.setStatus(AdoptionRequest.Status.Requested);
        request.setUserRequested(user_normal);
        request.setDate_created(new Date());
        adoptionRequestRepository.save(request);

    }


}
