package ro.proiect.epa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.proiect.epa.model.AnimalMedicalHistory;
import ro.proiect.epa.model.AnimalType;
import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.User;

import java.util.List;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {

    List<Animal> findByAnimalType(AnimalType animalType);
    List<Animal> findByUserAdded(User user);
    //Animal findByAnimalMedicalHistory( AnimalMedicalHistory animalMedicalHistory);
}
