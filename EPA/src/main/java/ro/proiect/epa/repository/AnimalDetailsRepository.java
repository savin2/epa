package ro.proiect.epa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.proiect.epa.model.AnimalDetails;

@Repository
public interface AnimalDetailsRepository extends JpaRepository<AnimalDetails, Long> {
    AnimalDetails findBySex(AnimalDetails.Sex sex);
}
