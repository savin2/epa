package ro.proiect.epa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.proiect.epa.model.AnimalMedicalHistory;

import java.util.List;


@Repository
public interface AnimalMedicalHistoryRepository extends JpaRepository<AnimalMedicalHistory, Long> {
   List<AnimalMedicalHistory> findByNeutering(AnimalMedicalHistory.Neutering neutering);
}
