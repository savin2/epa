package ro.proiect.epa.controller;

import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.proiect.epa.commands.ChangePasswordCommand;
import ro.proiect.epa.model.User;
import ro.proiect.epa.model.UserProfile;
import ro.proiect.epa.service.UserProfileService;
import ro.proiect.epa.service.UserService;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/profile")
public class ProfileController {

    private UserProfileService userProfileService;
    private UserService userService;

    public ProfileController(UserProfileService userProfileService, UserService userService) {
        this.userProfileService = userProfileService;
        this.userService = userService;
    }

    @GetMapping("/view")
    public String listLinks(Model model, Principal principal) {

        String email=principal.getName();
        User signedUser = userService.getUserByEmail(email);

        UserProfile profile = userProfileService.findByUser(signedUser);
        UserProfile nullProfile = new UserProfile();

        if(profile == null) {
            nullProfile.setLastName("No value");
            nullProfile.setFirstName("No value");
            nullProfile.setPhoneNumber("No value");
            nullProfile.setCountry("No value");
            nullProfile.setUser(signedUser);

            model.addAttribute("userProfile", nullProfile);
        }

        else {
            model.addAttribute("userProfile", profile);
        }

        return "profile/view";
    }

    @GetMapping("/edit")
    public String editProfile(Model model, Principal principal) {

        String email=principal.getName();
        User signedUser = userService.getUserByEmail(email);

        UserProfile profile = userProfileService.findByUser(signedUser);
        UserProfile nullProfile = new UserProfile();

        if(profile == null) {
            model.addAttribute("userProfile", nullProfile);
        }
        else {
            model.addAttribute("userProfile", profile);
        }

        return "profile/edit";
    }

    @PostMapping("/save")
    public String saveProfile(@Valid UserProfile userProfile, BindingResult bindingResult, Principal principal) {

        String email=principal.getName();
        User signedUser = userService.getUserByEmail(email);

        if (bindingResult.hasErrors()) {
            return "profile/edit";
        }
        else {

            userProfile.setUser(signedUser);
            userProfileService.createUserProfile(userProfile);

            return "redirect:/profile/view";
        }
    }

    @GetMapping("/changePassword")
    public String changePassword(Model model, Principal principal) {

        String email=principal.getName();
        User signedUser = userService.getUserByEmail(email);

        ChangePasswordCommand changePasswordCommand = new ChangePasswordCommand();
        changePasswordCommand.setId(signedUser.getId());

        model.addAttribute("changePasswordCommand", changePasswordCommand);

        return "profile/change_password";
    }

    @PostMapping("/saveNewPassword")
    public String saveNewPassword(@Valid ChangePasswordCommand changePasswordCommand, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "profile/change_password";
        }

        if (userService.isOldPasswordWrong(changePasswordCommand)) {
            bindingResult.rejectValue("oldPassword", "error.changePasswordCommand", "Old Password is incorrect!");
            return "profile/change_password";
        }

        System.out.println(changePasswordCommand.toString());
        userService.changePassword(changePasswordCommand);
        return "redirect:/";
    }
}
