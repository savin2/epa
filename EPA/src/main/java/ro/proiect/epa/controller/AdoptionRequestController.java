package ro.proiect.epa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.proiect.epa.model.AdoptionRequest;
import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.User;
import ro.proiect.epa.service.AdoptionRequestService;
import ro.proiect.epa.service.AnimalService;
import ro.proiect.epa.service.AnimalTypeService;
import ro.proiect.epa.service.UserService;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/adoptionRequests")
public class AdoptionRequestController {

    @Autowired
    private AdoptionRequestService adoptionRequestService;
    @Autowired
    private UserService userService;

    public AdoptionRequestController(AdoptionRequestService adoptionRequestService, UserService userService) {
        this.adoptionRequestService = adoptionRequestService;
        this.userService = userService;
    }

    @GetMapping("")
    public String findAll(Model model, Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        List<AdoptionRequest> requests = adoptionRequestService.findAll();

        model.addAttribute("requests", requests);

        return "adoptionRequest/adoptionRequests";
    }

    @GetMapping("/my")
    public String myAdoptions(Model model, Principal principal, SecurityContextHolderAwareRequestWrapper request) {

        String email=principal.getName();
        User signedUser = userService.getUserByEmail(email);

        List<AdoptionRequest> requests = adoptionRequestService.findByUser(signedUser);

        model.addAttribute("requests", requests);

        return "adoptionRequest/adoptionRequests";
    }

    @GetMapping("/adopt")
    public String createAdoptionRequest(@RequestParam("animalId") Long animalId, Model model, Principal principal){

        String email=principal.getName();
        User signedUser = userService.getUserByEmail(email);
        if(adoptionRequestService.canRequest(animalId, signedUser)) {
            adoptionRequestService.create(animalId, signedUser);
        }
        return "redirect:/animals/list";
    }

    @GetMapping("/approve")
    public String Approve(@RequestParam("id") Long id, Model model, Principal principal){
        String email=principal.getName();
        User signedUser = userService.getUserByEmail(email);
        adoptionRequestService.approve(id, signedUser);
        return "redirect:/adoptionRequests";
    }

    @GetMapping("/decline")
    public String Decline(@RequestParam("id") Long id, Model model, Principal principal){
        String email=principal.getName();
        User signedUser = userService.getUserByEmail(email);
        adoptionRequestService.decline(id, signedUser);
        return "redirect:/adoptionRequests";
    }

}
