package ro.proiect.epa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ro.proiect.epa.model.User;
import ro.proiect.epa.service.UserService;

import javax.validation.Valid;

@Controller
public class RegisterController {

    private UserService userService;

    @Autowired
    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterForm(Model model) {
        model.addAttribute("user", new User());
        return "registration";
    }

    @PostMapping("/register")
    public String registerUser(@Valid User user, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        if (userService.isUserEmailPresent(user.getEmail())) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "*There is already a user registered with the email provided");
            return "registration";
        }

        if (userService.isUserNamePresent(user.getUsername())) {
            bindingResult
                    .rejectValue("username", "error.user",
                            "*There is already a user registered with the username provided");
            return "registration";
        }

        userService.createUser(user);
        return "redirect:/register?success";
    }

}
