package ro.proiect.epa.controller;

import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.AnimalType;
import ro.proiect.epa.model.User;
import ro.proiect.epa.service.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/animals")
public class AnimalController {

    private AnimalService animalService;
    private UserService userService;
    private AnimalTypeService animalTypeService;

    public AnimalController(AnimalService animalService, UserService userService, AnimalTypeService animalTypeService) {
        this.animalService = animalService;
        this.userService = userService;
        this.animalTypeService = animalTypeService;
    }

    @GetMapping("/list")
    public String listLinks(Model model) {

        List<Animal> theAnimals = animalService.findAll();

        model.addAttribute("animals", theAnimals);
        model.addAttribute("animalTypes", animalTypeService.findAll());

        return "animals/animal_list";
    }

    @GetMapping("/animalType/{description}")
    public String listLinksByCategory(@PathVariable String description, Model model) {
        AnimalType theAnimalType = animalTypeService.getAnimalTypeByName(description);
        List<Animal> theAnimals = animalService.findByAnimalType(theAnimalType);

        model.addAttribute("animals", theAnimals);
        model.addAttribute("animalTypes", animalTypeService.findAll());

        return "animals/animal_list";
    }

    @GetMapping("/add")
    public String createLink(Model model) {

        Animal animal = new Animal();

        model.addAttribute("animal", animal);

        return "animals/animal_form";
    }

    @GetMapping("/update")
    public String updateLink(@RequestParam("animalId") Long theId, Model model) {

        Animal animal = animalService.findById(theId);

        model.addAttribute("animal", animal);

        return "animals/animal_form";
    }

    @PostMapping("/save")
    public String saveEmployee(@Valid Animal animal, BindingResult bindingResult, Principal principal) {

        String email=principal.getName();
        User signedUser = userService.getUserByEmail(email);

        if (bindingResult.hasErrors()) {
            return "animals/animal_form";
        }
        else {
            animal.setUserAdded(signedUser);
            animalService.createAnimal(animal);

            return "redirect:/animals/list";
        }
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("animalId") Long id) {

        animalService.deleteById(id);
        return "redirect:/animals/list";

    }

    @GetMapping("/addAnimalType")
    public String addCategory(@RequestParam("animalId") Long theId, Model model){

        model.addAttribute("animalTypes", animalTypeService.findAll());
        model.addAttribute("animal", animalService.findById(theId));
        return "animals/animal_type_form";
    }

    @GetMapping("/{id}/animalTypes")
    public String studentsAddCourse(@PathVariable Long id, @RequestParam Long animalId) {

        AnimalType animalType = animalTypeService.findById(animalId);
        Animal animal = animalService.findById(id);

        animal.setAnimalType(animalType);
        animalService.createAnimal(animal);

        return "redirect:/animals/list";
    }


}
