package ro.proiect.epa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.AnimalMedicalHistory;
import ro.proiect.epa.service.AnimalMedicalHistoryService;
import ro.proiect.epa.service.AnimalService;
import ro.proiect.epa.service.AnimalTypeService;
import ro.proiect.epa.service.UserService;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/animalMedicalHistory")
public class AnimalMedicalHistoryController {
    private AnimalService animalService;
    private UserService userService;
    private AnimalTypeService animalTypeService;
    private AnimalMedicalHistoryService animalMedicalHistoryService;

    public AnimalMedicalHistoryController(AnimalService animalService, UserService userService, AnimalTypeService animalTypeService, AnimalMedicalHistoryService animalMedicalHistoryService) {
        this.animalService = animalService;
        this.userService = userService;
        this.animalTypeService = animalTypeService;
        this.animalMedicalHistoryService = animalMedicalHistoryService;
    }

    @GetMapping("/viewAnimalMedicalHistory")
    public String viewAnimalMedicalHistory(@RequestParam Long animalId, Model model) {

        AnimalMedicalHistory animalMedicalHistory = animalMedicalHistoryService.findById(animalId);

        Animal animal = animalService.findById(animalId);

        model.addAttribute("animal", animal);
        model.addAttribute("animalMedicalHistory", animalMedicalHistory);

        return "animalMedicalHistory/viewMedicalHistory";
    }

}
