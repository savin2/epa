package ro.proiect.epa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.proiect.epa.model.AnimalType;
import ro.proiect.epa.service.AnimalTypeService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/animalTypes")
public class AnimalTypeController {

    private AnimalTypeService animalTypeService;

    public AnimalTypeController(AnimalTypeService animalTypeService) {
        this.animalTypeService = animalTypeService;
    }

    @GetMapping("/list")
    public String listLinks(Model model) {

        List<AnimalType> animalTypes = animalTypeService.findAll();

        model.addAttribute("animalTypes", animalTypes);

        return "animalTypes/animalType_list";
    }

    @GetMapping("/add")
    public String createLink(Model model) {

        AnimalType animalType = new AnimalType();

        model.addAttribute("animalType", animalType);

        return "animalTypes/animalType_form";
    }

    @GetMapping("/update")
    public String updateLink(@RequestParam("animalTypeId") Long theId, Model model) {

        AnimalType animalType = animalTypeService.findById(theId);

        model.addAttribute("animalType", animalType);

        return "animalTypes/animalType_form";
    }

    @PostMapping("/save")
    public String saveAnimalType(@Valid AnimalType animalType, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "animalTypes/animalType_form";
        }
        else {
            animalTypeService.createAnimalType(animalType);

            return "redirect:/animalTypes/list";
        }
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("animalTypeId") Long id) {

        animalTypeService.deleteById(id);

        return "redirect:/animalTypes/list";

    }
}
