package ro.proiect.epa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.AnimalDetails;
import ro.proiect.epa.service.AnimalDetailsService;
import ro.proiect.epa.service.AnimalService;
import ro.proiect.epa.service.AnimalTypeService;
import ro.proiect.epa.service.UserService;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/animalDetails")
public class AnimalDetailsController {
    private AnimalService animalService;
    private UserService userService;
    private AnimalTypeService animalTypeService;
    private AnimalDetailsService animalDetailsService;

    public AnimalDetailsController(AnimalService animalService, UserService userService, AnimalTypeService animalTypeService, AnimalDetailsService animalDetailsService) {
        this.animalService = animalService;
        this.userService = userService;
        this.animalTypeService = animalTypeService;
        this.animalDetailsService = animalDetailsService;
    }

    @GetMapping("/viewAnimalDetails")
    public String viewAnimalDetails(@RequestParam Long animalId, Model model) {

        AnimalDetails animalDetails = animalDetailsService.findById(animalId);

        Animal animal = animalService.findById(animalId);

        model.addAttribute("animal", animal);
        model.addAttribute("animalDetails", animalDetails);

        return "animalDetails/viewAnimalDetails";
    }

}
