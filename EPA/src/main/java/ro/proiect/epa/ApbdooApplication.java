package ro.proiect.epa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApbdooApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApbdooApplication.class, args);
	}

}
