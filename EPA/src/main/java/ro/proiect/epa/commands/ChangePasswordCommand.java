package ro.proiect.epa.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ChangePasswordCommand {
    private Long id;

    @Length(min = 5, message = "*Your old password must have at least 5 characters")
    @NotEmpty(message = "*Please provide your old password")
    private String oldPassword;

    @Length(min = 5, message = "*Your new password must have at least 5 characters")
    @NotEmpty(message = "*Please provide your new password")
    private String newPassword;
}
