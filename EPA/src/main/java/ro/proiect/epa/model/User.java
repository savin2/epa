package ro.proiect.epa.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Email(message = "*Please provide a valid Email")
	@NotEmpty(message = "*Please provide an email")
	private String email;

	@NotEmpty(message = "*Please provide a username")
	private String username;

	@Length(min = 5, message = "*Your password must have at least 5 characters")
	@NotEmpty(message = "*Please provide your password")
	private String password;

	@OneToOne(mappedBy="user")
	private UserProfile userProfile;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "userAdded")
	private Set<Animal> animalsAdded = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "userRequested")
	private Set<AdoptionRequest> adoptionRequest = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "userAdmin")
	private Set<AdoptionRequest> adoptionRequestByAdmin = new HashSet<>();

	@ManyToMany
	@JoinTable(
			name="user_role",
			joinColumns=@JoinColumn(name="user_id"),
			inverseJoinColumns=@JoinColumn(name="role_id")
			)	
	private List<Role> roles;

	public User(@NotEmpty(message = "*Please provide a username") String username,
				@Email(message = "*Please provide a valid Email")
				@NotEmpty(message = "*Please provide an email") String email,
				@Length(min = 5, message = "*Your password must have at least 5 characters")
				@NotEmpty(message = "*Please provide your password") String password) {
		this.username = username;
		this.email = email;
		this.password = password;
	}
}




