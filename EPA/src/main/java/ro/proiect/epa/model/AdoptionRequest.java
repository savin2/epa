package ro.proiect.epa.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class AdoptionRequest {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_created;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_closed;

    public enum Status {
        Requested, Approved, Declined
    }

    @Enumerated(EnumType.STRING)
    Status status;

    @ManyToOne
    private User userRequested;

    @OneToOne
    @JoinColumn(name="animal_id")
    private Animal animal;

    @ManyToOne
    @JoinColumn(name="user_admin_id")
    private User userAdmin;

}
