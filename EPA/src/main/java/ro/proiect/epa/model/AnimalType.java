package ro.proiect.epa.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class AnimalType {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @NotEmpty(message = "*Please provide name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "animalType")
    private Set<Animal> animalsAdded = new HashSet<>();

    public AnimalType(@NotEmpty(message = "*Please provide name") String name) {
        this.name = name;
    }
}
