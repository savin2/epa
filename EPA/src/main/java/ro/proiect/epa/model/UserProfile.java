package ro.proiect.epa.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Getter
@Setter
public class UserProfile {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @NotEmpty(message = "*Please provide your first name")
    private String firstName;

    @NotEmpty(message = "*Please provide your last name")
    private String lastName;

    @NotEmpty(message = "*Please provide your phone number")
    private String phoneNumber;

    @NotEmpty(message = "*Please provide your country")
    private String country;

    @OneToOne
    @JoinColumn(name="user_profile_id")
    private User user;
}
