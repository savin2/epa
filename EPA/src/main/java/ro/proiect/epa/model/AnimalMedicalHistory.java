package ro.proiect.epa.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class AnimalMedicalHistory {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    private String known_disease;

    @DateTimeFormat(pattern = "yyyy-MM-DD")
    private Date date_from_last_vaccine;

    public enum Neutering {
        Yes, No
    }

    @Enumerated(EnumType.STRING)
    Neutering neutering;

    @OneToOne(mappedBy="animalMedicalHistory")
    private Animal animal;
    
}
