package ro.proiect.epa.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class AnimalDetails {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    private String description;

    @DateTimeFormat(pattern = "yyyy-MM-DD")
    private Date arrival_date;

    @DateTimeFormat(pattern = "yyyy-MM-DD")
    private Date estimated_birth_date;

    public enum Sex {
        Male, Female
    }

    @Enumerated(EnumType.STRING)
    Sex sex;

    @OneToOne(mappedBy="animalDetails")
    private Animal animal;

}
