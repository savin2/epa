package ro.proiect.epa.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Animal {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "*Please provide title")
    private String title;

    @NotEmpty(message = "*Please provide url")
    private String url;

    @ManyToOne
    private User userAdded;

    @ManyToOne
    private AnimalType animalType;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="animal_medical_history_id")
    private AnimalMedicalHistory animalMedicalHistory;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="animal_details_id")
    private AnimalDetails animalDetails;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="animal")
    private Set<AdoptionRequest> adoptionRequest = new HashSet();

    public boolean canAdopt(String email){
        return adoptionRequest.stream().noneMatch(ar -> ar.getUserRequested().getEmail().equals(email));
    }

}
