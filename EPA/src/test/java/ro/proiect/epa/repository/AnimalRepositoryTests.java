package ro.proiect.epa.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ro.proiect.epa.model.Animal;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class AnimalRepositoryTests {

    @Autowired
    private AnimalRepository animalRepository;

    @Test
    public void findAnimalById() {
        Optional<Animal> animal = animalRepository.findById(1L);
        assertEquals("Caine bland in doua culori", animal.get().getTitle());
        assertEquals("https://www.animalzoo.ro/wp-content/uploads/2017/05/cu11-604x515.jpg", animal.get().getUrl());
    }
}
