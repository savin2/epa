package ro.proiect.epa.controllers;

import org.hamcrest.beans.HasProperty;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ro.proiect.epa.controller.AnimalMedicalHistoryController;
import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.AnimalMedicalHistory;
import ro.proiect.epa.model.AnimalType;
import ro.proiect.epa.service.AnimalMedicalHistoryService;
import ro.proiect.epa.service.AnimalService;
import ro.proiect.epa.service.AnimalTypeService;

import javax.servlet.Filter;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("dev")
public class AnimalMedicalHistoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private AnimalService animalService;


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(springSecurityFilterChain)
                .apply(springSecurity())
                .build();
    }


    @Test
    public void testViewAdminAnimalHistory3() throws Exception {

        mockMvc.perform(get("/animalMedicalHistory/viewAnimalMedicalHistory?animalId=3").with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animalMedicalHistory/viewMedicalHistory"))
                .andReturn();

    }
    @Test
    public void testViewAnimalHistory3() throws Exception {
        Animal animal =  animalService.findById(3L);
        AnimalMedicalHistory animalMedicalHistory = animal.getAnimalMedicalHistory();

        mockMvc.perform(get("/animalMedicalHistory/viewAnimalMedicalHistory?animalId=3").with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animalMedicalHistory/viewMedicalHistory"))
                .andExpect(model().attribute("animalMedicalHistory",hasProperty( "known_disease",is(animalMedicalHistory.getKnown_disease()))));

    }

}
