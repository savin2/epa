package ro.proiect.epa.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ro.proiect.epa.model.AnimalType;

import javax.servlet.Filter;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("dev")
public class AnimalTypeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(springSecurityFilterChain)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void testListAnimalTypes() throws Exception {
        mockMvc.perform(get("/animalTypes/list")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animalTypes/animalType_list"))
                .andExpect(model().attributeExists("animalTypes"));
    }

    @Test
    public void testCreateAnimalType() throws Exception {
        mockMvc.perform(get("/animalTypes/add")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animalTypes/animalType_form"))
                .andExpect(model().attributeExists("animalType"));
    }

    @Test
    public void testUpdateAnimalType() throws Exception {
        mockMvc.perform(get("/animalTypes/update")
                .param("animalTypeId", "5")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animalTypes/animalType_form"))
                .andExpect(model().attributeExists("animalType"));
    }

    @Test
    public void testSaveAnimalType() throws Exception {
        AnimalType animalType = new AnimalType();
        animalType.setName("Musculita");
        mockMvc.perform(post("/animalTypes/save").with(user("normal@normal.com").password("normal@normal.com").roles("USER"))
                .flashAttr("animalType", animalType))
                .andExpect(status().isFound());
    }

    @Test
    public void testSaveAnimalTypeWithErrors() throws Exception {
        AnimalType animalType = new AnimalType();
        animalType.setName("");
        mockMvc.perform(post("/animalTypes/save").with(user("normal@normal.com").password("normal@normal.com").roles("USER"))
                .flashAttr("animalType", animalType))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteAction() throws Exception {
        mockMvc.perform(get("/animalTypes/delete/")
                .param("animalTypeId", "5")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/animalTypes/list"));
    }

}
