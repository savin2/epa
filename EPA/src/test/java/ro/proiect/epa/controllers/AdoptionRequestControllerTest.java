package ro.proiect.epa.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("dev")
public class AdoptionRequestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(springSecurityFilterChain)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void testFindAll() throws Exception {
        mockMvc.perform(get("/adoptionRequests").with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("adoptionRequest/adoptionRequests"))
                .andExpect(model().attribute("requests", hasSize(1)));
    }

    public void testFindByUser() throws Exception {
        mockMvc.perform(get("/adoptionRequests/my").with(user("normal@normal.com").password("normal@normal.com").roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("adoptionRequest/adoptionRequests"))
                .andExpect(model().attribute("requests", hasSize(1)));
    }

    @Test
    public void testCreateAdoption() throws Exception{
        mockMvc.perform(get("/adoptionRequests/adopt")
                .param("animalId", "3")
                .with(user("normal@normal.com").password("normal@normal.com").roles("USER")))
                .andExpect(view().name("redirect:/animals/list"));
    }

    @Test
    public void testApprove() throws  Exception {
        mockMvc.perform(get("/adoptionRequests/approve")
                .param("id", "1")
                .with(user("normal@normal.com").password("normal@normal.com").roles("ADMIN")))
                .andExpect(view().name("redirect:/adoptionRequests"));
    }

    @Test
    public void testDecline() throws  Exception {
        mockMvc.perform(get("/adoptionRequests/decline")
                .param("id", "1")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(view().name("redirect:/adoptionRequests"));
    }

}
