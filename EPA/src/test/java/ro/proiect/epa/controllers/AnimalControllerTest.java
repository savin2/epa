package ro.proiect.epa.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("dev")
public class AnimalControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setup() {

        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(springSecurityFilterChain)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void testListAnimals() throws Exception {
        mockMvc.perform(get("/animals/list")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animals/animal_list"))
                .andExpect(model().attributeExists("animals"))
                .andExpect(model().attributeExists("animalTypes"));
    }

    @Test
    public void testSearchAnimalsByType() throws Exception {
        mockMvc.perform(get("/animals/animalType/Caine")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animals/animal_list"))
                .andExpect(model().attributeExists("animals"))
                .andExpect(model().attributeExists("animalTypes"));
    }

    @Test
    public void testCreateAnimal() throws Exception {
        mockMvc.perform(get("/animals/add")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animals/animal_form"))
                .andExpect(model().attributeExists("animal"));
    }

    @Test
    public void testUpdateAnimal() throws Exception {
        mockMvc.perform(get("/animals/update")
                .param("animalId", "1")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animals/animal_form"))
                .andExpect(model().attributeExists("animal"));
    }

    @Test
    public void testSaveAnimal() throws Exception{
        mockMvc.perform(post("/animals/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "")
                .param("title", "Title")
                .param("url", "http://www.google.com/")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/animals/list"));
    }

    @Test
    public void testSaveAnimalWithErrors() throws Exception{
        mockMvc.perform(post("/animals/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "")
                .param("title", "")
                .param("url", "http://www.google.com/")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("animals/animal_form"));
    }

    @Test
    public void testDeleteAction() throws Exception {
        mockMvc.perform(get("/animals/delete/")
                .param("animalId", "2")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/animals/list"));
    }

    @Test
    public void testAnimalAddTypeForm() throws Exception {
        mockMvc.perform(get("/animals/addAnimalType")
                .param("animalId", "1")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("animals/animal_type_form"))
                .andExpect(model().attributeExists("animal"))
                .andExpect(model().attributeExists("animalTypes"));
    }

    @Test
    public void testAnimalAddType() throws Exception {
        mockMvc.perform(post("/animals/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "")
                .param("title", "Title")
                .param("url", "http://www.google.com/")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/animals/list"));

        mockMvc.perform(get("/animals/3/animalTypes")
                .param("animalId", "1")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/animals/list"));
    }
}
