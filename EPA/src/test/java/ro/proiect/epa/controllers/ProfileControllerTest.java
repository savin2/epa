package ro.proiect.epa.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ro.proiect.epa.model.User;
import ro.proiect.epa.model.UserProfile;

import javax.servlet.Filter;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("dev")
public class ProfileControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setup() {

        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(springSecurityFilterChain)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void testFindByUser() throws Exception {
        mockMvc.perform(get("/profile/view")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("profile/view"))
                .andExpect(model().attributeExists("userProfile"));
    }

    @Test
    public void testViewWithNoProfile() throws Exception {
        mockMvc.perform(get("/profile/view")
                .with(user("user@email.com").password("user@email.com").roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("profile/view"))
                .andExpect(model().attributeExists("userProfile"));
    }

    @Test
    public void testEditProfile() throws Exception {
        mockMvc.perform(get("/profile/edit")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("profile/edit"))
                .andExpect(model().attributeExists("userProfile"));
    }

    @Test
    public void testEditWithNoProfile() throws Exception {
        mockMvc.perform(get("/profile/edit")
                .with(user("user@email.com").password("user@email.com").roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("profile/edit"))
                .andExpect(model().attributeExists("userProfile"));
    }

    @Test
    public void testSaveProfile() throws Exception{
        mockMvc.perform(post("/profile/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "1")
                .param("firstName", "First")
                .param("lastName", "Last")
                .param("phoneNumber", "0712345678")
                .param("country", "Romania")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/profile/view"));
    }

    @Test
    public void testSaveProfileWithErrors() throws Exception{
        mockMvc.perform(post("/profile/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "2")
                .param("firstName", "")
                .param("lastName", "Last2")
                .param("phoneNumber", "")
                .param("country", "Romania")
                .with(user("user2@email.com").password("user2@email.com").roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("profile/edit"));
    }

    @Test
    public void testChangePassword() throws Exception {
        mockMvc.perform(get("/profile/changePassword")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("profile/change_password"))
                .andExpect(model().attributeExists("changePasswordCommand"));
    }

    @Test
    public void testSaveNewPassword() throws Exception{
        mockMvc.perform(post("/profile/saveNewPassword")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "1")
                .param("oldPassword", "admin@admin.com")
                .param("newPassword", "123456")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));
    }

    @Test
    public void testSaveNewPasswordWithWrongOldPassword() throws Exception{
        mockMvc.perform(post("/profile/saveNewPassword")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "1")
                .param("oldPassword", "wrong")
                .param("newPassword", "123456")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("profile/change_password"));
    }

    @Test
    public void testSaveNewPasswordWithWrongInputs() throws Exception{
        mockMvc.perform(post("/profile/saveNewPassword")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "1")
                .param("newPassword", "456")
                .with(user("admin@admin.com").password("admin@admin.com").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(view().name("profile/change_password"));
    }

}
