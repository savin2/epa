package ro.proiect.epa.services;

import org.junit.Test;
import java.util.*;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ro.proiect.epa.model.AdoptionRequest;
import ro.proiect.epa.model.Animal;
import ro.proiect.epa.model.AnimalMedicalHistory;
import ro.proiect.epa.model.User;
import ro.proiect.epa.repository.AdoptionRequestRepository;
import ro.proiect.epa.repository.AnimalMedicalHistoryRepository;
import ro.proiect.epa.repository.UserRepository;
import ro.proiect.epa.service.AdoptionRequestService;
import ro.proiect.epa.service.AnimalMedicalHistoryService;
import ro.proiect.epa.service.AnimalService;
import ro.proiect.epa.service.AnimalTypeService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class AnimalMedicalHistoryServiceTest {

    public static final Long animalId = 3L;
    public static final AnimalMedicalHistory.Neutering neutering= AnimalMedicalHistory.Neutering.Yes;
    public static final List<String> diseases = new ArrayList<>();


    @Autowired
    AnimalMedicalHistoryService animalMedicalHistoryService;

    @Autowired
    AnimalService animalService;
    @Autowired
    AnimalTypeService animalTypeService;

    @Autowired
    AnimalMedicalHistoryRepository animalMedicalHistoryRepository;

    @Test
    public void testCreateAnimalMedicalHistory(){
        Animal animalTest = new Animal();
        animalTest.setUrl("URL TEST");
        animalTest.setTitle("Animal de Test");
        animalTest.setAnimalType(animalTypeService.getAnimalTypeByName("Peste"));

        animalService.createAnimal(animalTest);


        AnimalMedicalHistory animalMedicalHistoryTest = new AnimalMedicalHistory();
        animalMedicalHistoryTest.setKnown_disease("Boala Incurabila");
        animalMedicalHistoryTest.setNeutering(neutering);
        animalMedicalHistoryTest.setAnimal(animalTest);
        AnimalMedicalHistory test =  animalMedicalHistoryService.createAnimalMedicalHistory(animalMedicalHistoryTest);

        assertEquals(test.getAnimal().getTitle(), "Animal de Test" );
        assertEquals(test.getAnimal().getUrl(), "URL TEST");
        assertEquals(test.getAnimal().getAnimalType().getName(), "Peste");

        assertEquals(test.getId(),animalMedicalHistoryTest.getId());
        assertEquals(test.getNeutering(), neutering);
        assertEquals(test.getKnown_disease(),"Boala Incurabila");

    }

    @Test
    public void testGetAnimalMedicalHistoryByNeutering(){
        diseases.add("Parvoviroza");
        diseases.add("Coronavirus");

        List<AnimalMedicalHistory> animalMedicalHistory =   animalMedicalHistoryService.getAnimalMedicalHistoryByNeutering(neutering);

        for(int i = 0 ; i<animalMedicalHistory.size()-1; i++){
            assertEquals(animalMedicalHistory.get(i).getKnown_disease(),diseases.get(i));
            assertNotNull(animalMedicalHistory.get(i).getDate_from_last_vaccine());
            assertNotNull(animalMedicalHistory.get(i).getAnimal());
        }
    }



}
