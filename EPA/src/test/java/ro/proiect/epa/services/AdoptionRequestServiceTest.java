package ro.proiect.epa.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ro.proiect.epa.model.AdoptionRequest;
import ro.proiect.epa.model.User;
import ro.proiect.epa.repository.AdoptionRequestRepository;
import ro.proiect.epa.repository.UserRepository;
import ro.proiect.epa.service.AdoptionRequestService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class AdoptionRequestServiceTest {

    public static final Long animalId = 3l;
    public static final Long userId = 1l;
    public static final Long adoptionRequestId = 1l;

    @Autowired
    AdoptionRequestService adoptionRequestService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AdoptionRequestRepository adoptionRequestRepository;

    @Test
    public void testCreateAdoptionRequest(){
        User user = userRepository.findById(userId).get();
        AdoptionRequest request = adoptionRequestService.create(animalId, user);
        assertEquals(request.getUserRequested().getId(), userId);
        assertEquals(request.getAnimal().getId(), animalId);
        assertEquals(request.getStatus(), AdoptionRequest.Status.Requested);
        assertNotNull(request.getDate_created());
    }

    @Test
    public void testApproveAdoptionRequest(){
        User user = userRepository.findById(userId).get();
        adoptionRequestService.approve(adoptionRequestId, user);
        AdoptionRequest request = adoptionRequestRepository.findById(adoptionRequestId).get();
        assertEquals(request.getStatus(), AdoptionRequest.Status.Approved);
        assertEquals(request.getUserAdmin().getId(), userId);
        assertNotNull(request.getDate_closed());
    }

    @Test
    public void testDeclineAdoptionRequest(){
        User user = userRepository.findById(userId).get();
        adoptionRequestService.decline(adoptionRequestId, user);
        AdoptionRequest request = adoptionRequestRepository.findById(adoptionRequestId).get();
        assertEquals(request.getStatus(), AdoptionRequest.Status.Declined);
        assertEquals(request.getUserAdmin().getId(), userId);
        assertNotNull(request.getDate_closed());
    }

    @Test
    public void testGetAdoptionRequestList(){
        List<AdoptionRequest> requests = adoptionRequestService.findAll();
        assertNotNull(requests);
    }

    @Test
    public void testGetAdoptionRequestListByUser(){
        User user = userRepository.findById(userId).get();
        List<AdoptionRequest> requests = adoptionRequestService.findByUser(user);
        boolean allSameUser = requests.stream().allMatch(r -> r.getUserRequested().getId().equals(user.getId()));
        assertEquals(allSameUser, true);
    }

    @Test
    public void testCannotRequestAdoptionTwice(){
        AdoptionRequest adoptionRequest = adoptionRequestRepository.findById(adoptionRequestId).get();
        boolean canRequest = adoptionRequestService.canRequest(adoptionRequest.getAnimal().getId(),adoptionRequest.getUserRequested());
        assertEquals(canRequest, false);
    }

}
