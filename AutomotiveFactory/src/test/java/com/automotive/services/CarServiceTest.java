package com.automotive.services;

import com.automotive.commands.CarCommand;
import com.automotive.converters.CarToCommandConverter;
import com.automotive.converters.CommandToCarConverter;
import com.automotive.converters.CommandToCarModelConverter;
import com.automotive.model.Car;
import com.automotive.repositories.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class CarServiceTest {

    public static final String vinNumber = "1111";

    @Autowired
    CarService carService;

    @Autowired
    CarRepository carRepository;

    @Autowired
    CarToCommandConverter carToCommandConverter;
    @Autowired
    CommandToCarConverter commandToCarConverter;

    @Test
    public void testSaveFactory(){
        log.info("Car save test");
        Car car = carRepository.findByVinNumber("0000");
        CarCommand carCommand = carToCommandConverter.convert(car);
        carCommand.setVinNumber(vinNumber);
        CarCommand savedCarCommand = carService.saveCarCommand(carCommand);
        assertEquals(vinNumber, savedCarCommand.getVinNumber());
        log.info("Factory save test");
    }

}
