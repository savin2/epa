package com.automotive.services;

import com.automotive.commands.PurchaseOrderCommand;
import com.automotive.converters.CommandToPurchaseOrderConverter;
import com.automotive.converters.PurchaseOrderToCommandConverter;
import com.automotive.model.PurchaseOrder;
import com.automotive.repositories.PurchaseOrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class PurchaseOrderServiceTest {
    public static final String code = "123";

    @Autowired
    PurchaseOrderService purchaseOrderService;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PurchaseOrderToCommandConverter purchaseOrderToCommandConverter;
    @Autowired
    CommandToPurchaseOrderConverter commandToPurchaseOrderConverter;

    @Test
    public void testSavePurchaseOrder(){
        log.info("PurchaseOrder save test");
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findByCode("321");
        PurchaseOrderCommand purchaseOrderCommand = purchaseOrderToCommandConverter.convert(purchaseOrder);
        purchaseOrderCommand.setCode(code);
        PurchaseOrderCommand savedPurchaseOrderCommand = purchaseOrderService.savePurchaseOrderCommand(purchaseOrderCommand);
        assertEquals(code, savedPurchaseOrderCommand.getCode());
    }
}
