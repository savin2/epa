package com.automotive.services;

import com.automotive.commands.PartCommand;
import com.automotive.converters.CommandToPartConverter;
import com.automotive.converters.PartToCommandConverter;
import com.automotive.model.Part;
import com.automotive.repositories.PartRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class PartServiceTest {

    public static final String name = "Hull";

    @Autowired
    PartService partService;

    @Autowired
    PartRepository partRepository;

    @Autowired
    PartToCommandConverter partToCommandConverter;
    @Autowired
    CommandToPartConverter commandToPartConverter;

    @Test
    public void testSavePart(){
        log.info("Part save test");
        Part part = partRepository.findByName("Engine");
        PartCommand partCommand = partToCommandConverter.convert(part);
        partCommand.setName(name);
        PartCommand savedPartCommand = partService.savePart(partCommand);
        assertEquals(name, savedPartCommand.getName());
    }

}
