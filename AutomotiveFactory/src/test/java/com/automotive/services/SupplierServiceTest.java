package com.automotive.services;


import com.automotive.commands.SupplierCommand;
import com.automotive.converters.CommandToSupplierConverter;
import com.automotive.converters.SupplierToCommandConverter;
import com.automotive.model.Supplier;
import com.automotive.repositories.SupplierRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class SupplierServiceTest {

    public static final String name = "Supplier";

    @Autowired
    SupplierService supplierService;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    SupplierToCommandConverter supplierToCommandConverter;
    @Autowired
    CommandToSupplierConverter commandToSupplierConverter;

    @Test
    public void testSaveFactory(){
        log.info("Supplier save test");
        Supplier supplier = supplierRepository.findByName("Arbath");
        SupplierCommand supplierCommand = supplierToCommandConverter.convert(supplier);
        supplierCommand.setName(name);
        SupplierCommand savedSupplierCommand = supplierService.saveSupplierCommand(supplierCommand);
        assertEquals(name, savedSupplierCommand.getName());
        log.info("Factory save test");
    }

}
