package com.automotive.services;


import com.automotive.commands.CarModelCommand;
import com.automotive.converters.CarModelToCommandConverter;
import com.automotive.converters.CommandToCarModelConverter;
import com.automotive.model.CarModel;
import com.automotive.repositories.CarModelRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class CarModelServiceTest {

    public static final String name = "Renault";

    @Autowired
    CarModelService carModelService;

    @Autowired
    CarModelRepository carModelRepository;

    @Autowired
    CarModelToCommandConverter carModelToCommandConverter;
    @Autowired
    CommandToCarModelConverter commandToCarModelConverter;

    @Test
    public void testSaveFactory(){
        log.info("CarModel save test");
        CarModel carModel = carModelRepository.findByName("Dacia");
        CarModelCommand carModelCommand = carModelToCommandConverter.convert(carModel);
        carModelCommand.setName(name);
        CarModelCommand savedCarModelCommand = carModelService.saveCarModelCommand(carModelCommand);
        assertEquals(name, savedCarModelCommand.getName());
        log.info("Factory save test");
    }


}
