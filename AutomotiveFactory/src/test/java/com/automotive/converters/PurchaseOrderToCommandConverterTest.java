package com.automotive.converters;

import com.automotive.commands.PurchaseOrderCommand;
import com.automotive.model.PurchaseOrder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class PurchaseOrderToCommandConverterTest {

    public static final Long id = 1L;
    public static final String code = "code";
    public static final double cost = 12345;
    public static final boolean completed = true;
    public static final Date requestDate = new Date(2018,11,11);

    public PurchaseOrderToCommandConverter purchaseOrderToCommandConverter;

    @Before
    public void initTest(){
        log.info("Before test");
        purchaseOrderToCommandConverter = new PurchaseOrderToCommandConverter();
    }

    @Test
    public void convertNull(){
        log.info("Convert null test");
        PurchaseOrderCommand purchaseOrderCommand = purchaseOrderToCommandConverter.convert(null);
        assertNull(purchaseOrderCommand);
    }

    @Test
    public void convert(){
        log.info("Purchase order to command convert test");
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setId(id);
        purchaseOrder.setCode(code);
        purchaseOrder.setCost(cost);
        purchaseOrder.setCompleted(completed);
        purchaseOrder.setRequestDate(requestDate);

        PurchaseOrderCommand purchaseOrderCommand = purchaseOrderToCommandConverter.convert(purchaseOrder);
        assertEquals(id, purchaseOrderCommand.getId());
        assertEquals(code, purchaseOrderCommand.getCode());
        assertEquals(cost, purchaseOrderCommand.getCost());
        assertEquals(completed, purchaseOrderCommand.isCompleted());
        assertEquals(requestDate, purchaseOrderCommand.getRequestDate());
    }

}
