package com.automotive.converters;

import com.automotive.commands.CarCommand;
import com.automotive.model.Car;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class CarToCommandConverterTest {
    public static final String vinNumber = "vinNumber";
    public static final Date fabricationDate = new Date(2018,11,11);

    CarToCommandConverter carToCommandConverter;

    @Before
    public void initTest(){
        log.info("Before test");
        carToCommandConverter = new CarToCommandConverter();
    }

    @Test
    public void convertNull(){
        log.info("Test convert null");
        CarCommand contactInfoCommand = carToCommandConverter.convert(null);
        assertNull(contactInfoCommand);
    }

    @Test
    public void convert(){
        log.info("Test convert car to command");
        Car car = new Car();
        car.setVinNumber(vinNumber);
        car.setFabricationDate(fabricationDate);
        CarCommand carCommand = carToCommandConverter.convert(car);
        assertEquals(vinNumber, carCommand.getVinNumber());
        assertEquals(fabricationDate, carCommand.getFabricationDate());
    }
}
