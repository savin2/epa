package com.automotive.converters;

import com.automotive.commands.PurchaseOrderCommand;
import com.automotive.model.PurchaseOrder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class CommandToPurchaseOrderConverterTest {

    public static final Long id = 1L;
    public static final String code = "code";
    public static final double cost = 12345;
    public static final boolean completed = true;
    public static final Date requestDate = new Date(2018,11,11);

    public CommandToPurchaseOrderConverter commandToPurchaseOrderConverter;

    @Before
    public void initTest(){
        log.info("Before test");
        commandToPurchaseOrderConverter = new CommandToPurchaseOrderConverter();
    }

    @Test
    public void convertNull(){
        log.info("Convert null test");
        PurchaseOrder purchaseOrder = commandToPurchaseOrderConverter.convert(null);
        assertNull(purchaseOrder);
    }

    @Test
    public void convert(){
        log.info("Purchase order to command convert test");
        PurchaseOrderCommand purchaseOrderCommand = new PurchaseOrderCommand();
        purchaseOrderCommand.setId(id);
        purchaseOrderCommand.setCode(code);
        purchaseOrderCommand.setCost(cost);
        purchaseOrderCommand.setCompleted(completed);
        purchaseOrderCommand.setRequestDate(requestDate);

        PurchaseOrder purchaseOrder = commandToPurchaseOrderConverter.convert(purchaseOrderCommand);
        assertEquals(id, purchaseOrder.getId());
        assertEquals(code, purchaseOrder.getCode());
        assertEquals(cost, purchaseOrder.getCost());
        assertEquals(completed, purchaseOrder.isCompleted());
        assertEquals(requestDate, purchaseOrder.getRequestDate());
    }

}
