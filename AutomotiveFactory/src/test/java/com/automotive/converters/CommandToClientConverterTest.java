package com.automotive.converters;

import com.automotive.commands.ClientCommand;
import com.automotive.model.Client;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class CommandToClientConverterTest {

    public static final Long id = 1L;
    public static final String name = "name";

    public CommandToClientConverter commandToClientConverter;

    @Before
    public void initTest(){
        log.info("Before client to command convert");
        commandToClientConverter = new CommandToClientConverter();
    }

    @Test
    public void convertNull(){
        log.info("Command to client null convert test");
        Client client = commandToClientConverter.convert(null);
        assertNull(client);
    }

    @Test
    public void convert() {
        log.info("Command to client convert test");
        ClientCommand clientCommand = new ClientCommand();
        clientCommand.setId(id);
        clientCommand.setName(name);

        Client client = commandToClientConverter.convert(clientCommand);
        assertEquals(id, client.getId());
        assertEquals(name, client.getName());
    }

}
