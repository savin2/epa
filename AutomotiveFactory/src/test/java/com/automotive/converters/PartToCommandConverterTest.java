package com.automotive.converters;

import com.automotive.commands.PartCommand;
import com.automotive.model.Part;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class PartToCommandConverterTest {

    public static final Long id = 1L;
    public static final String name = "name";
    public static final int quantity = 100000;
    public static final String description = "Description description!@#$#@@#$$&@&~";

    public PartToCommandConverter partToCommandConverter;

    @Before
    public void initTest(){
        log.info("Before part to command convert");
        partToCommandConverter = new PartToCommandConverter();
    }

    @Test
    public void convertNull(){
        log.info("Part to command null convert test");
        PartCommand partCommand = partToCommandConverter.convert(null);
        assertNull(partCommand);
    }

    @Test
    public void convert(){
        log.info("Part to command convert test");
        Part part = new Part();
        part.setId(id);
        part.setName(name);
        part.setQuantity(quantity);
        part.setDescription(description);

        PartCommand partCommand = partToCommandConverter.convert(part);
        assertEquals(id, partCommand.getId());
        assertEquals(name, partCommand.getName());
        assertEquals(quantity, partCommand.getQuantity());
        assertEquals(description, partCommand.getDescription());

    }

}
