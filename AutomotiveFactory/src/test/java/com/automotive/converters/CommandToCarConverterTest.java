package com.automotive.converters;

import com.automotive.commands.CarCommand;
import com.automotive.model.Car;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class CommandToCarConverterTest {

    public static final Long id = 1L;
    public static final String vinNumber = "vinNumber";
    public static final Date fabricationDate = new Date(2018,11,11);

    CommandToCarConverter commandToCarConverter;

    @Before
    public void initTest(){
        log.info("Before test");
        commandToCarConverter = new CommandToCarConverter();
    }

    @Test
    public void convertNull(){
        log.info("Test convert null");
        Car car = commandToCarConverter.convert(null);
        assertNull(car);
    }

    @Test
    public void convert(){
        log.info("Test convert car to command");
        CarCommand carCommand = new CarCommand();
        carCommand.setVinNumber(vinNumber);
        carCommand.setFabricationDate(fabricationDate);
        Car car = commandToCarConverter.convert(carCommand);
        assertEquals(vinNumber, car.getVinNumber());
        assertEquals(fabricationDate, car.getFabricationDate());
    }

}
