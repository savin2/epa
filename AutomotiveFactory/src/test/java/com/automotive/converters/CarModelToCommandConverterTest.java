package com.automotive.converters;

import com.automotive.commands.CarModelCommand;
import com.automotive.model.CarModel;
import com.automotive.model.CarType;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class CarModelToCommandConverterTest {

    public static final Long id = 1L;
    public static final String name = "dacia";
    public static final double basePrice = 4333;
    public static final CarType type = CarType.Sedan;
    public static final byte[] image = new byte[1000];

    CarModelToCommandConverter carModelToCommandConverter;

    @Before
    public void initTest(){
        log.info("Before test");
        carModelToCommandConverter = new CarModelToCommandConverter();
    }

    @Test
    public void convertNull(){
        log.info("Test convert null");
        CarModelCommand carModelCommand = carModelToCommandConverter.convert(null);
        assertNull(carModelCommand);
    }

    @Test
    public void convert(){
        log.info("Test convert carModel to command");
        CarModel carModel = new CarModel();
        carModel.setId(id);
        carModel.setName(name);
        carModel.setType(type);
        carModel.setBasePrice(basePrice);
        carModel.setImage(image);

        CarModelCommand carModelCommand = carModelToCommandConverter.convert(carModel);
        assertEquals(id, carModelCommand.getId());
        assertEquals(name, carModelCommand.getName());
        assertEquals(type, carModelCommand.getType());
        assertEquals(basePrice, carModelCommand.getBasePrice());
        assertEquals(image, carModelCommand.getImage());
    }

}
