package com.automotive.converters;

import com.automotive.commands.FactoryCommand;
import com.automotive.model.Factory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.*;
import static junit.framework.TestCase.assertEquals;

@Slf4j
public class CommandToFactoryConverterTest {

    public static final String name = "Mioveni";
    public static final Long id = 1L;

    CommandToFactoryConverter commandToFactoryConverter;

    @Before
    public void initTest(){
        log.info("Before command to factory test");
        commandToFactoryConverter = new CommandToFactoryConverter();
    }

    @Test
    public void convertNull(){
        log.info("Test convert command to factory null");
        Factory factory = commandToFactoryConverter.convert(null);
        assertNull(factory);
    }

    @Test
    public void convert(){
        log.info("Test convert command to factory");
        FactoryCommand factoryCommand = new FactoryCommand();
        factoryCommand.setName(name);
        factoryCommand.setId(id);
        Factory factory = commandToFactoryConverter.convert(factoryCommand);

        assertNotNull(factoryCommand);
        assertEquals(factoryCommand.getId(), id);
        assertEquals(factory.getName(), name);
    }

}