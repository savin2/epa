package com.automotive.converters;

import com.automotive.commands.ClientCommand;
import com.automotive.model.Client;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class ClientToCommandConverterTest {

    public static final Long id = 1L;
    public static final String name = "name";

    public ClientToCommandConverter clientToCommandConverter;

    @Before
    public void initTest(){
        log.info("Before client to command convert");
        clientToCommandConverter = new ClientToCommandConverter();
    }

    @Test
    public void convertNull(){
        log.info("Client to command null convert test");
        ClientCommand clientCommand = clientToCommandConverter.convert(null);
        assertNull(clientCommand);
    }

    @Test
    public void convert() {
        log.info("Client to command convert test");
        Client client = new Client();
        client.setId(id);
        client.setName(name);

        ClientCommand clientCommand = clientToCommandConverter.convert(client);
        assertEquals(id, clientCommand.getId());
        assertEquals(name, clientCommand.getName());
    }

}
