package com.automotive.converters;

import com.automotive.commands.CarModelCommand;
import com.automotive.model.CarModel;
import com.automotive.model.CarType;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class CommandToCarModelConverterTest {

    public static final Long id = 1L;
    public static final String name = "dacia";
    public static final double basePrice = 4333;
    public static final CarType type = CarType.Sedan;
    public static final byte[] image = new byte[1000];

    CommandToCarModelConverter commandToCarModelConverter;

    @Before
    public void initTest(){
        log.info("Before test");
        commandToCarModelConverter = new CommandToCarModelConverter();
    }

    @Test
    public void convertNull(){
        log.info("Test convert null");
        CarModel carModel = commandToCarModelConverter.convert(null);
        assertNull(carModel);
    }

    @Test
    public void convert(){
        log.info("Test convert command to car model");
        CarModelCommand carModelCommand = new CarModelCommand();
        carModelCommand.setId(id);
        carModelCommand.setName(name);
        carModelCommand.setType(type);
        carModelCommand.setBasePrice(basePrice);
        carModelCommand.setImage(image);

        CarModel carModel = commandToCarModelConverter.convert(carModelCommand);
        assertEquals(id, carModel.getId());
        assertEquals(name, carModel.getName());
        assertEquals(type, carModel.getType());
        assertEquals(basePrice, carModel.getBasePrice());
        assertEquals(image, carModel.getImage());
    }
}
