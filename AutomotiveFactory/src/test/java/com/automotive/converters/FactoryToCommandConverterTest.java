package com.automotive.converters;

import com.automotive.commands.FactoryCommand;
import com.automotive.model.Factory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.exceptions.ExceptionIncludingMockitoWarnings;

import static junit.framework.TestCase.*;

@Slf4j
public class FactoryToCommandConverterTest {

    public static final String name = "Mioveni";
    public static final Long id = 1L;

    FactoryToCommandConverter factoryToCommandConverter;

    @Before
    public void initTest(){
        log.info("Before factory command test");
        factoryToCommandConverter = new FactoryToCommandConverter();
    }

    @Test
    public void convertNull(){
        log.info("Test convert factory to command null");
        FactoryCommand factoryCommand = factoryToCommandConverter.convert(null);
        assertNull(factoryCommand);
    }

    @Test
    public void convert(){
        log.info("Test convert factory to command");
        Factory factory = new Factory();
        factory.setName(name);
        factory.setId(id);
        FactoryCommand factoryCommand = factoryToCommandConverter.convert(factory);

        assertNotNull(factoryCommand);
        assertEquals(factoryCommand.getId(), id);
        assertEquals(factory.getName(), name);
    }

}
