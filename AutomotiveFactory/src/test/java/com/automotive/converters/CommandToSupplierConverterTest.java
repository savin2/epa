package com.automotive.converters;

import com.automotive.commands.SupplierCommand;
import com.automotive.model.Supplier;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class CommandToSupplierConverterTest {

    public static final Long id = 1L;
    public static final String name = "name";

    CommandToSupplierConverter commandToSupplierConverter;

    @Before
    public void initTest(){
        log.info("Before command to supplier test");
        commandToSupplierConverter = new CommandToSupplierConverter();
    }

    @Test
    public void convertNull(){
        log.info("Command to supplier null convert");
        Supplier supplier = commandToSupplierConverter.convert(null);
        assertNull(supplier);
    }

    @Test
    public void convert(){
        log.info("Command to supplier converter test");
        SupplierCommand supplierCommand = new SupplierCommand();
        supplierCommand.setId(id);
        supplierCommand.setName(name);

        Supplier supplier = commandToSupplierConverter.convert(supplierCommand);
        assertEquals(id, supplier.getId());
        assertEquals(name, supplier.getName());
    }

}
