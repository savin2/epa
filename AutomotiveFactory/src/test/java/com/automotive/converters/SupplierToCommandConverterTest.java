package com.automotive.converters;

import com.automotive.commands.SupplierCommand;
import com.automotive.model.Supplier;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
public class SupplierToCommandConverterTest {

    public static final Long id = 1L;
    public static final String name = "name";

    SupplierToCommandConverter supplierToCommandConverter;

    @Before
    public void initTest(){
        log.info("Before supplier to command test");
        supplierToCommandConverter = new SupplierToCommandConverter();
    }

    @Test
    public void convertNull(){
        log.info("Supplier to command null convert");
        SupplierCommand supplierCommand = supplierToCommandConverter.convert(null);
        assertNull(supplierCommand);
    }

    @Test
    public void convert(){
        log.info("Supplier to command converter test");
        Supplier supplier = new Supplier();
        supplier.setId(id);
        supplier.setName(name);

        SupplierCommand supplierCommand = supplierToCommandConverter.convert(supplier);
        assertEquals(id, supplierCommand.getId());
        assertEquals(name, supplierCommand.getName());
    }

}
