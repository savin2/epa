insert into car (vin_number, fabrication_date) VALUES ('0000', TO_DATE('11/06/2019', 'DD/MM/YYYY'))
insert into car_model (name, base_price, type) VALUES ('Dacia', 7000, 'Sedan')
insert into factory (name) VALUES ('Mioveni')
insert into client (name) VALUES ('Iatsa')
insert into purchase_order (code, cost, completed, client_id, factory_id) VALUES ('321', 2344, 0, 1, 1)
update car set car_model_id = 1, purchase_order_id = 1 WHERE id = 1
insert into supplier (name) VALUES ('Arbath')
insert into part (name, quantity) VALUES ('Engine', 23)