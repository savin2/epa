package com.automotive.services;

import com.automotive.commands.FactoryCommand;
import com.automotive.model.Factory;

import java.util.Set;

public interface FactoryService {

    Set<FactoryCommand> getFactories();
    Factory getFactory(Long id);
    Factory save(Factory factory);
    void delete(Long id);
    void deleteCarModel(Long id, Long carModelId);
    FactoryCommand getFactoryCommand(Long id);
    FactoryCommand saveFactoryCommand(FactoryCommand factoryCommand);

}
