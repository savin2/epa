package com.automotive.services;

import com.automotive.commands.PartCommand;
import com.automotive.model.Part;

import java.util.Set;

public interface PartService {
    Set<Part> getParts();
    PartCommand getPart(Long id);
    PartCommand savePart(PartCommand partCommand);
    void delete(Long id);
}
