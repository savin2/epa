package com.automotive.services;

import com.automotive.commands.PartCommand;
import com.automotive.converters.CommandToPartConverter;
import com.automotive.converters.PartToCommandConverter;
import com.automotive.model.Part;
import com.automotive.model.Supplier;
import com.automotive.repositories.PartRepository;
import com.automotive.repositories.SupplierRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class PartServiceImpl implements PartService {

    PartRepository partRepository;
    SupplierRepository supplierRepository;
    PartToCommandConverter partToCommandConverter;
    CommandToPartConverter commandToPartConverter;

    public Set<Part> getParts(){
        Set<Part> parts = new HashSet<>();
        partRepository.findAll().iterator().forEachRemaining(parts::add);
        return parts;
    }

    public PartCommand getPart(Long id){
        Optional<Part> partOptional = partRepository.findById(id);
        if(!partOptional.isPresent()){
            throw new RuntimeException("Part not found");
        }
        return partToCommandConverter.convert(partOptional.get());
    }

    public PartCommand savePart(PartCommand partCommand){
        Part part = commandToPartConverter.convert(partCommand);
        Set<Supplier> suppliers = new HashSet<>();
        supplierRepository.findAllById(partCommand.getSupplierIds()).iterator().forEachRemaining(suppliers::add);
        part.setSuppliers(suppliers);
        return partToCommandConverter.convert(partRepository.save(part));
    }

    public void delete(Long id){
        partRepository.deleteById(id);
    }

}
