package com.automotive.services;

import com.automotive.commands.PurchaseOrderCommand;
import com.automotive.model.PurchaseOrder;

import java.util.Set;

public interface PurchaseOrderService {

    Set<PurchaseOrder> getPurchaseOrders();
    PurchaseOrder getPurchaseOrder(Long id);
    PurchaseOrderCommand getPurchaseOrderCommand(Long id);
    void deletePurchaseOrder(Long id);
    PurchaseOrderCommand savePurchaseOrderCommand(PurchaseOrderCommand order);

}
