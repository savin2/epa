package com.automotive.services;

import com.automotive.commands.CarModelCommand;
import com.automotive.model.CarModel;

import java.util.Set;

public interface CarModelService {

    Set<CarModel> getCarModels();
    CarModel getCarModel(Long id);
    CarModel saveCarModel(CarModel car);
    CarModelCommand getCarModelCommand(Long id);
    CarModelCommand saveCarModelCommand(CarModelCommand carModelCommand);
    void deleteCarModel(Long id);
}
