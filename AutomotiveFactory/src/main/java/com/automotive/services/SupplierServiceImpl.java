package com.automotive.services;

import com.automotive.commands.SupplierCommand;
import com.automotive.converters.CommandToSupplierConverter;
import com.automotive.converters.SupplierToCommandConverter;
import com.automotive.model.Part;
import com.automotive.model.Supplier;
import com.automotive.repositories.PartRepository;
import com.automotive.repositories.SupplierRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class SupplierServiceImpl implements SupplierService {

    SupplierRepository supplierRepository;
    PartRepository partRepository;
    SupplierToCommandConverter supplierToCommandConverter;
    CommandToSupplierConverter commandToSupplierConverter;

    public Set<Supplier> getSuppliers(){
        Set<Supplier> suppliers = new HashSet<>();
        supplierRepository.findAll().iterator().forEachRemaining(suppliers::add);
        return suppliers;
    }

    public Supplier getSupplier(Long id){
        Optional<Supplier> supplierOptional = supplierRepository.findById(id);
        if(!supplierOptional.isPresent()){
            throw new RuntimeException("Supplier not found");
        }
        return supplierOptional.get();
    }

    public SupplierCommand getSupplierCommand(Long id){
        return supplierToCommandConverter.convert(getSupplier(id));
    }

    public Supplier save(Supplier supplier){
        return supplierRepository.save(supplier);
    }

    public SupplierCommand saveSupplierCommand(SupplierCommand supplierCommand){
        Supplier supplier = commandToSupplierConverter.convert(supplierCommand);
        Set<Part> parts = new HashSet<>();
        partRepository.findAllById(supplierCommand.getPartIds()).iterator().forEachRemaining(parts::add);
        supplier.setParts(parts);
        return supplierToCommandConverter.convert(save(supplier));
    }

    public void delete(Long id){
        supplierRepository.deleteById(id);
    }

}
