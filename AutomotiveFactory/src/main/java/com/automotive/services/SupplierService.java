package com.automotive.services;

import com.automotive.commands.SupplierCommand;
import com.automotive.model.Supplier;

import java.util.Set;

public interface SupplierService {
    Set<Supplier> getSuppliers();
    Supplier getSupplier(Long id);
    SupplierCommand getSupplierCommand(Long id);
    Supplier save(Supplier supplier);
    SupplierCommand saveSupplierCommand(SupplierCommand supplierCommand);
    void delete(Long id);
}
