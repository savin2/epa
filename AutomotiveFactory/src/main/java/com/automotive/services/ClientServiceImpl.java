package com.automotive.services;

import com.automotive.commands.ClientCommand;
import com.automotive.converters.ClientToCommandConverter;
import com.automotive.converters.CommandToClientConverter;
import com.automotive.model.Client;
import com.automotive.repositories.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    ClientRepository clientRepository;
    ClientToCommandConverter clientToCommandConverter;
    CommandToClientConverter commandToClientConverter;
    EntityManager entityManager;

    @Override
    public Set<Client> getClients() {
        Set<Client> clients = new HashSet<>();
        clientRepository.findAll().iterator().forEachRemaining(clients::add);

        for(Client c : clients){
            c.setName(clientRepository.decrypt(c.getName()));
            c.setCNP(clientRepository.decrypt(c.getCNP()));
        }
        return clients;
    }

    @Override
    public Client getClient(Long l) {

        Optional<Client> clientOptional = clientRepository.findById(l);

        if (!clientOptional.isPresent()) {
            throw new RuntimeException("Client not found!");
        }

        Client client = clientOptional.get();
        client.setName(clientRepository.decrypt(client.getName()));
        client.setCNP(clientRepository.decrypt(client.getCNP()));

        return clientOptional.get();
    }

    @Override
    public ClientCommand getClientCommand(Long l){
        return clientToCommandConverter.convert(getClient(l));
    }


    @Override
    public Client saveClient(Client client) {
        client = clientRepository.save(client);
        clientRepository.encrypt(client.getId());
        return client;
    }

    @Override
    public ClientCommand saveClientCommand(ClientCommand clientCommand){
        Client client = saveClient(commandToClientConverter.convert(clientCommand));
        return clientToCommandConverter.convert(client);
    }

    @Override
    public void delete(Long id) {
        clientRepository.deleteById(id);
    }

    @Override
    public void createContext() {
        entityManager.createNamedStoredProcedureQuery("ADMINISTRATOR.PROCED_TIME_CTX").execute();
    }

}
