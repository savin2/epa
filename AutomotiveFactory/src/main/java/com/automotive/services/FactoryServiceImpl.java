package com.automotive.services;

import com.automotive.commands.FactoryCommand;
import com.automotive.converters.CommandToFactoryConverter;
import com.automotive.converters.FactoryToCommandConverter;
import com.automotive.model.CarModel;
import com.automotive.repositories.CarModelRepository;
import com.automotive.repositories.FactoryRepository;
import com.automotive.model.Factory;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class FactoryServiceImpl implements FactoryService{

    FactoryRepository factoryRepository;
    CarModelRepository carModelRepository;
    CommandToFactoryConverter commandToFactoryConverter;
    FactoryToCommandConverter factoryToCommandConverter;

    @Override
    public Set<FactoryCommand> getFactories() {
        Set<Factory> factories = new HashSet<>();
        factoryRepository.findAll().iterator().forEachRemaining(factories::add);
        return factories.stream().map(f -> factoryToCommandConverter.convert(f)).collect(Collectors.toSet());
    }

    @Override
    public Factory getFactory(Long l) {

        Optional<Factory> factoryOptional = factoryRepository.findById(l);

        if (!factoryOptional.isPresent()) {
            throw new RuntimeException("Factory not found!");
        }

        return factoryOptional.get();
    }

    @Override
    public FactoryCommand getFactoryCommand(Long l){
        Factory factory = getFactory(l);
        FactoryCommand factoryCommand = factoryToCommandConverter.convert(factory);
        return factoryCommand;
    }

    @Override
    public FactoryCommand saveFactoryCommand(FactoryCommand f){
        log.info("Saving factory command");
        Factory factory = commandToFactoryConverter.convert(f);
        Set<CarModel> carModels = new HashSet<>();
        carModelRepository.findAllById(f.getSelectedCarModels()).iterator().forEachRemaining(carModels::add);
        factory.setCarModels(carModels);
        return factoryToCommandConverter.convert(save(factory));
    }

    @Override
    public Factory save(Factory factory) {
        return factoryRepository.save(factory);
    }

    @Override
    public void delete(Long id) {
        factoryRepository.deleteById(id);
    }

    @Override
    public void deleteCarModel(Long id, Long carModelId){
        Factory factory = getFactory(id);
        factory.setCarModels(factory.getCarModels().stream().filter(carModel -> carModel.getId() != carModelId).collect(Collectors.toSet()));
        save(factory);
    }

}
