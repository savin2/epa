package com.automotive.services;

import com.automotive.commands.PurchaseOrderCommand;
import com.automotive.converters.CommandToPurchaseOrderConverter;
import com.automotive.converters.PurchaseOrderToCommandConverter;
import com.automotive.model.PurchaseOrder;
import com.automotive.repositories.PurchaseOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    PurchaseOrderRepository purchaseOrderRepository;
    PurchaseOrderToCommandConverter purchaseOrderToCommandConverter;
    CommandToPurchaseOrderConverter commandToPurchaseOrderConverter;

    @Override
    public Set<PurchaseOrder> getPurchaseOrders() {
        Set<PurchaseOrder> purchaseOrders = new HashSet<PurchaseOrder>();
        purchaseOrderRepository.findAll().iterator().forEachRemaining(purchaseOrders::add);
        return purchaseOrders;
    }

    @Override
    public PurchaseOrder getPurchaseOrder(Long l){

        Optional<PurchaseOrder> purchaseOrderOptional = purchaseOrderRepository.findById(l);

        if (!purchaseOrderOptional.isPresent()) {
            throw new RuntimeException("Order not found!");
        }
        return purchaseOrderOptional.get();
    }

    @Override
    public PurchaseOrderCommand getPurchaseOrderCommand(Long l) {
        return purchaseOrderToCommandConverter.convert(getPurchaseOrder(l));
    }


    @Override
    public PurchaseOrderCommand savePurchaseOrderCommand(PurchaseOrderCommand purchaseOrderCommand) {
        PurchaseOrder purchaseOrdrer = purchaseOrderRepository.save(commandToPurchaseOrderConverter.convert(purchaseOrderCommand));
        return purchaseOrderToCommandConverter.convert(purchaseOrdrer);
    }

    @Override
    public void deletePurchaseOrder(Long id) {
        purchaseOrderRepository.deleteById(id);
    }

}
