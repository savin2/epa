package com.automotive.services;

import com.automotive.commands.CarCommand;
import com.automotive.model.Car;

import java.util.Set;

public interface CarService {

    Set<Car> getCars();
    Car getCar(Long id);
    Car saveCar(Car car);
    CarCommand getCarCommand(Long id);
    CarCommand saveCarCommand(CarCommand carCommand);
    void deleteCar(Long id);

}
