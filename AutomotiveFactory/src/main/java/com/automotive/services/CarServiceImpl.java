package com.automotive.services;

import com.automotive.commands.CarCommand;
import com.automotive.converters.CarToCommandConverter;
import com.automotive.converters.CommandToCarConverter;
import com.automotive.model.Car;
import com.automotive.repositories.CarRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CarServiceImpl implements CarService {

    CarRepository carRepository;
    CarToCommandConverter carToCommandConverter;
    CommandToCarConverter commandToCarConverter;

    @Override
    public Set<Car> getCars() {
        Set<Car> cars = new HashSet<Car>();
        carRepository.findAll().iterator().forEachRemaining(cars::add);
        return cars;
    }

    @Override
    public Car getCar(Long l) {

        Optional<Car> carOptional = carRepository.findById(l);

        if (!carOptional.isPresent()) {
            throw new RuntimeException("Car not found!");
        }

        return carOptional.get();
    }

    @Override
    public CarCommand getCarCommand(Long l){
        return carToCommandConverter.convert(getCar(l));
    }


    @Override
    public Car saveCar(Car car) {
        Car savedCar = carRepository.save(car);
        return savedCar;
    }

    @Override
    public CarCommand saveCarCommand(CarCommand carCommand){
        Car car = saveCar(commandToCarConverter.convert(carCommand));
        return carToCommandConverter.convert(car);
    }

    @Override
    public void deleteCar(Long id) {
        carRepository.deleteById(id);
    }

}
