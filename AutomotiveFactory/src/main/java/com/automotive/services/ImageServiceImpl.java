package com.automotive.services;

import com.automotive.model.CarModel;
import com.automotive.repositories.CarModelRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImageServiceImpl implements ImageService {

    CarModelRepository carModelRepository;

    public ImageServiceImpl(CarModelRepository carModelRepository){
        this.carModelRepository = carModelRepository;
    }

    @Override @Transactional
    public void saveImageFile(Long carModelId, MultipartFile file) {
        try { CarModel carModel = carModelRepository.findById(carModelId).get();
            carModel.setImage(file.getBytes());
            carModelRepository.save(carModel); }
        catch (IOException e) {
        }
    }
}
