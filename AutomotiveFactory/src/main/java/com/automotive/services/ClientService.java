package com.automotive.services;

import com.automotive.commands.ClientCommand;
import com.automotive.model.Client;

import java.util.Set;

public interface ClientService {

    Set<Client> getClients();
    Client getClient(Long id);
    ClientCommand getClientCommand(Long id);
    Client saveClient(Client client);
    ClientCommand saveClientCommand(ClientCommand clientCommand);
    void delete(Long id);
    void createContext();

}
