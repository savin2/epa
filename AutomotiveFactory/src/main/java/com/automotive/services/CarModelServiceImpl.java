package com.automotive.services;

import com.automotive.commands.CarModelCommand;
import com.automotive.converters.CarModelToCommandConverter;
import com.automotive.converters.CommandToCarModelConverter;
import com.automotive.model.CarModel;
import com.automotive.repositories.CarModelRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class CarModelServiceImpl implements CarModelService{

    CarModelRepository carModelRepository;
    CarModelToCommandConverter carModelToCommandConverter;
    CommandToCarModelConverter commandToCarModelConverter;

    @Override
    public Set<CarModel> getCarModels() {
        Set<CarModel> cars = new HashSet<CarModel>();
        carModelRepository.findAll().iterator().forEachRemaining(cars::add);
        return cars;
    }

    @Override
    public CarModel getCarModel(Long l) {

        Optional<CarModel> carModelOptional = carModelRepository.findById(l);

        if (!carModelOptional.isPresent()) {
            throw new RuntimeException("Car model not found!");
        }

        return carModelOptional.get();
    }

    @Override
    public CarModelCommand getCarModelCommand(Long l) {
        return carModelToCommandConverter.convert(getCarModel(l));
    }


    @Override
    public CarModel saveCarModel(CarModel car) {
        CarModel savedCar = carModelRepository.save(car);
        return savedCar;
    }

    @Override
    public CarModelCommand saveCarModelCommand(CarModelCommand carModelCommand){
        return carModelToCommandConverter.convert(saveCarModel(commandToCarModelConverter.convert(carModelCommand)));
    }

    @Override
    public void deleteCarModel(Long id) {
        carModelRepository.deleteById(id);
    }

}
