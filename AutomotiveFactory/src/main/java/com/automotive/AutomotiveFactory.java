package com.automotive;

import com.automotive.config.SecurityWebApplicationInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomotiveFactory {

    public static void main(String[] args) {
        SpringApplication.run(AutomotiveFactory.class, args);
    }
}
