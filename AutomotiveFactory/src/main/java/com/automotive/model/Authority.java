package com.automotive.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name="AUTHORITIES",indexes = {@Index(name = "ix_auth_username", columnList = "username", unique = true),
        @Index(name = "ix_auth_username", columnList = "authority", unique = true)})
public class Authority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Column(length = 50)
    private String authority;
    @ManyToOne
    @JoinColumn(name="username")
    private User user;
}
