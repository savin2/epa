package com.automotive.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name="USERS")
public class User {
    @Id
    @Column(length = 50)
    private String username;
    @NotNull
    @Column(length = 100)
    private String password;
    @NotNull
    private int enabled;
    @OneToMany
    @JoinColumn(name="username")
    private Set<Authority> authorities = new HashSet<Authority>();
}
