package com.automotive.model;

import javax.persistence.*;


@Table(name="CAR_TYPE")
public enum CarType {

    @Enumerated(value = EnumType.STRING)
    Sedan, Hatchback, Break, Coupe, Cabriolet, Van, Utilitary, SUV

}
