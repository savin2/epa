package com.automotive.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name="PURCHASE_ORDER")
public class PurchaseOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date requestDate;
    private boolean completed;
    private double cost;

    @ManyToOne
    private Client client;

    @ManyToOne
    private Factory factory;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "purchaseOrder")
    private Set<Car> cars = new HashSet<Car>();

}
