package com.automotive.model;

import lombok.*;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name="CAR_MODEL")
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private CarType type;
    private Double basePrice;
    @Lob
    private byte[] image;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carModel")
    private Set<Car> cars = new HashSet<>();
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Part> parts = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "CarModelFactory",
            joinColumns = @JoinColumn(name = "car_model_id"),
            inverseJoinColumns = @JoinColumn(name = "factory_id"))
    private Set<Factory> factories = new HashSet<>();
}
