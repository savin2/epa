package com.automotive.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name="CAR")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String vinNumber;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fabricationDate;
    @ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private CarModel carModel;
    @ManyToOne
    private PurchaseOrder purchaseOrder;

}


