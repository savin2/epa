package com.automotive.model;

import lombok.*;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name="CLIENT")
@NamedStoredProcedureQuery(
        name="ADMINISTRATOR.PROCED_TIME_CTX",
        procedureName="ADMINISTRATOR.PROCED_TIME_CTX"
)
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthdate;
    private String CNP;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private Set<PurchaseOrder> purchaseOrders = new HashSet<>();


}
