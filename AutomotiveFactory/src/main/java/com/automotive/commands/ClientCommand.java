package com.automotive.commands;

import com.automotive.model.PurchaseOrder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class ClientCommand {

    private Long id;
    @NotNull(message = "required field")
    @Size(min = 2, message = "required")
    private String name;
    @NotNull(message = "required field")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthdate;
    @Range(min = 1000000000000L, max = 9999999999999L)
    @NotNull(message = "required field")
    @Length(min = 13,max = 13)
    private String CNP;
    private Set<PurchaseOrder> purchaseOrders = new HashSet<>();

}