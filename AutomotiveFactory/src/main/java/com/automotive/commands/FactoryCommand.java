package com.automotive.commands;

import com.automotive.model.CarModel;
import com.automotive.model.PurchaseOrder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class FactoryCommand {

    private Long id;
    @NotNull(message = "required field")
    @Size(min = 2, message = "required")
    private String name;
    private String country;
    private List<Long> selectedCarModels = new ArrayList<>();
    private Set<PurchaseOrder> purchaseOrders = new HashSet<>();
    private Set<CarModel> carModels = new HashSet<>();

}
