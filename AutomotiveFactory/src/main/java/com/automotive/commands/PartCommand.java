package com.automotive.commands;

import com.automotive.model.Factory;
import com.automotive.model.Supplier;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class PartCommand {

    private Long id;
    @NotNull(message = "required field")
    @Size(min = 2, message = "required")
    private String name;
    private String description;
    @Min(value=0, message = "Part quantity cannot be negative")
    private int quantity;
    @NotNull(message = "required field")
    private Factory factory;
    private Set<Long> supplierIds = new HashSet<>();
    private Set<Supplier> suppliers = new HashSet<>();


}
