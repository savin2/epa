package com.automotive.commands;

import com.automotive.model.CarModel;
import com.automotive.model.PurchaseOrder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class CarCommand {

    private Long id;
    @NotNull(message = "required field")
    @Size(min = 2, max = 30, message = "required")
    private String vinNumber;
    @NotNull(message = "required field")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fabricationDate;
    private CarModel carModel;
    private PurchaseOrder purchaseOrder;

}
