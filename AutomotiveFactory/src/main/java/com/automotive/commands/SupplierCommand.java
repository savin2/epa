package com.automotive.commands;

import com.automotive.model.Part;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class SupplierCommand {

    private Long id;
    @NotNull(message = "required field")
    @Size(min = 2, message = "name too short")
    private String name;
    @NotNull(message = "required field")
    @Size(min = 2, message = "name too short")
    private String address;
    @NotNull(message = "required field")
    @Email
    private String email;
    private Set<Long> partIds = new HashSet<>();

}
