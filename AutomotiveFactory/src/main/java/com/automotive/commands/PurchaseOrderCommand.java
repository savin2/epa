package com.automotive.commands;

import com.automotive.model.Client;
import com.automotive.model.Factory;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class PurchaseOrderCommand {

    private Long id;

    @NotNull(message = "Required")
    @Length(min = 3, message = "Length must be greater than 3")
    private String code;

    @NotNull(message = "Required")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date requestDate;
    private boolean completed;
    @Min(value = 0, message = "Cost must be positive")
    private double cost;

    private Client client;
    private Factory factory;

}
