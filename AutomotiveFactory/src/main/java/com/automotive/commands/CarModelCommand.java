package com.automotive.commands;

import com.automotive.model.Car;
import com.automotive.model.CarType;
import com.automotive.model.Factory;
import com.automotive.model.Part;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class CarModelCommand {

    private Long id;
    @NotNull(message = "required field")
    @Size(min = 2, message = "required")
    private String name;
    @Min(value=0, message = "min value 1000")
    @Max(value=20000000, message = "max value 200000000")
    private double basePrice;
    private byte[] image;
    private CarType type;
    private Set<Car> cars = new HashSet<>();
    private Set<Part> parts = new HashSet<>();
    private Set<Factory> factories = new HashSet<>();

}
