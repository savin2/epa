package com.automotive.config;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource securityDataSource;
    public PasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(securityDataSource)
               .withUser("admin").password(passwordEncoder().encode("admin")).roles("ADMIN", "CLIENT")
                .and().withUser("client").password(passwordEncoder().encode("client")).roles("CLIENT")
                .and().withUser("user").password(passwordEncoder().encode("user")).roles("USER");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/resources/**", "/static/**","/webjars/**").permitAll()
                .antMatchers("/carModels**").hasRole("ADMIN")
                .antMatchers("/factories**").hasRole("ADMIN")
                .antMatchers("/orders**").hasRole("CLIENT").anyRequest().authenticated().
                and().exceptionHandling()
                .accessDeniedPage("/accessDenied")
                .and()
                .formLogin()
                .loginPage("/showLogInForm")
                .failureUrl("/showErrorLogIn")
                .loginProcessingUrl("/authUser")
                .defaultSuccessUrl("/index", true)
                .permitAll().
                and().logout().logoutUrl("/logout").permitAll();
    }
}