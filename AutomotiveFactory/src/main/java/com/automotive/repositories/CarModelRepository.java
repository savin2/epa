package com.automotive.repositories;

import com.automotive.model.CarModel;
import org.springframework.data.repository.CrudRepository;

public interface CarModelRepository extends CrudRepository<CarModel, Long> {
    CarModel findByName(String name);
}
