package com.automotive.repositories;

import com.automotive.model.PurchaseOrder;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseOrderRepository extends CrudRepository<PurchaseOrder, Long> {
    PurchaseOrder findByCode(String code);
}
