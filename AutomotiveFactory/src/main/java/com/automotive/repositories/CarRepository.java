package com.automotive.repositories;

import com.automotive.model.Car;
import org.springframework.data.repository.CrudRepository;

public interface CarRepository extends CrudRepository<Car, Long> {
    Car findByVinNumber(String vinNumber);
}
