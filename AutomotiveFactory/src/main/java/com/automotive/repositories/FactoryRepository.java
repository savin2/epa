package com.automotive.repositories;
import com.automotive.model.Factory;

import org.springframework.data.repository.CrudRepository;

public interface FactoryRepository extends CrudRepository<Factory, Long> {
    Factory findByName(String name);
}
