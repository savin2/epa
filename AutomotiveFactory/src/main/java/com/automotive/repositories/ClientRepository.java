package com.automotive.repositories;

import com.automotive.model.Client;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface ClientRepository extends CrudRepository<Client, Long> {
    Client findByName(String name);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Client SET name  = Administrator.cript_text(name), CNP = Administrator.cript_text(cnp) WHERE ID = ?1", nativeQuery = true)
    void encrypt(Long Id);

    @Query(value="SELECT Administrator.decript_text(?1) FROM DUAL", nativeQuery = true)
    String decrypt(String input);
}
