package com.automotive.repositories;

import com.automotive.model.Part;
import org.springframework.data.repository.CrudRepository;

public interface PartRepository extends CrudRepository<Part,Long> {
    Part findByName(String name);
}
