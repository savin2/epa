package com.automotive.repositories;

import com.automotive.model.Supplier;
import org.springframework.data.repository.CrudRepository;

public interface SupplierRepository extends CrudRepository<Supplier, Long> {
    Supplier findByName(String name);
}
