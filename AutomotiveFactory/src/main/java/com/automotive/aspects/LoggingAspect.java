package com.automotive.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
@Order(1)
public class LoggingAspect {

    @Before("execution(public void saveFactory(..))")
    public void beforeSaveFactory() {log.info("Before factory save...");}

    @After("execution(public void com.automotive.*.FactoryServiceImpl.saveFactoryCommand(..))")
    public void afterFactorySave() {log.info("After factory save ...");}

    @After("execution(* com.automotive.model.*.*(..))")
    public void afterModelAdvice() {log.info("after model ...");}

}
