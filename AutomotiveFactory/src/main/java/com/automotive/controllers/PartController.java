package com.automotive.controllers;

import com.automotive.commands.PartCommand;
import com.automotive.model.Part;
import com.automotive.services.FactoryService;
import com.automotive.services.PartService;
import com.automotive.services.SupplierService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class PartController {

    private final PartService partService;
    private final FactoryService factoryService;
    private final SupplierService supplierService;

    public PartController(PartService partService, FactoryService factoryService, SupplierService supplierService){
        this.partService = partService;
        this.factoryService = factoryService;
        this.supplierService = supplierService;
    }

    @RequestMapping("parts")
    public String getParts(Model model){
        model.addAttribute("parts", partService.getParts());
        return "parts";
    }

    @RequestMapping("parts/new")
    public String addPart(Model model){
        model.addAttribute("part", new PartCommand());
        model.addAttribute("factories", factoryService.getFactories());
        model.addAttribute("suppliers", supplierService.getSuppliers());
        return "partForm";
    }

    @RequestMapping("parts/{id}/edit")
    public String editPart(@PathVariable String id, Model model){
        model.addAttribute("part", partService.getPart(Long.valueOf(id)));
        model.addAttribute("factories", factoryService.getFactories());
        model.addAttribute("suppliers", supplierService.getSuppliers());
        return "partForm";
    }

    @PostMapping("parts")
    public String savePart(@Valid @ModelAttribute PartCommand partCommand, BindingResult bindingResult){

        if(bindingResult.hasErrors())
            return "error";

        partService.savePart(partCommand);
        return "redirect:/parts";
    }

    @GetMapping("parts/{id}/delete")
    public String deletePart(@PathVariable String id){
        partService.delete(Long.valueOf(id));
        return "redirect:/parts";
    }

}
