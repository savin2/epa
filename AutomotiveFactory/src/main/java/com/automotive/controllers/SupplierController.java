package com.automotive.controllers;

import com.automotive.commands.SupplierCommand;
import com.automotive.model.Supplier;
import com.automotive.services.PartService;
import com.automotive.services.SupplierService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
public class SupplierController {

    private final SupplierService supplierService;
    private final PartService partService;

    @RequestMapping({"/suppliers"})
    public String getSuppliers(Model model){
        model.addAttribute("suppliers", supplierService.getSuppliers());
        return "suppliers";
    }

    @RequestMapping({"/suppliers/new"})
    public String addSupplier(Model model){
        model.addAttribute("supplier", new SupplierCommand());
        model.addAttribute("parts", partService.getParts());
        return "supplierForm";
    }

    @RequestMapping({"suppliers/{id}/edit"})
    public String editSupplier(@PathVariable String id, Model model){
        model.addAttribute("supplier", supplierService.getSupplierCommand(Long.valueOf(id)));
        model.addAttribute("parts", partService.getParts());
        return "supplierForm";
    }

    @PostMapping({"/suppliers"})
    public String saveSupplier(@Valid @ModelAttribute SupplierCommand supplierCommand, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "error";
        }

        supplierService.saveSupplierCommand(supplierCommand);
        return "redirect:/suppliers";
    }

    @RequestMapping({"/suppliers/{id}/delete"})
    public String deleteSupplier(@PathVariable String id){
        supplierService.delete(Long.valueOf(id));
        return "redirect:/suppliers";
    }
}
