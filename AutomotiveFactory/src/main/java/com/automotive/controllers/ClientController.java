package com.automotive.controllers;

import com.automotive.commands.ClientCommand;
import com.automotive.services.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
public class ClientController {

    private final ClientService clientService;

    @RequestMapping({"/clients"})
    public String getClients(Model m){
        m.addAttribute("clients",clientService.getClients());
        return "clients";
    }

    @RequestMapping({"/clients/new"})
    public String addClient(Model m){
        m.addAttribute("client", new ClientCommand());
        return "clientForm";
    }

    @RequestMapping({"/clients/{id}/edit"})
    public String editClient(@PathVariable String id, Model m){
        m.addAttribute("client", clientService.getClientCommand(Long.valueOf(id)));
        return "clientForm";
    }

    @PostMapping({"clients"})
    public String saveClient(@Valid @ModelAttribute ClientCommand clientCommand, BindingResult bindingResult){

        if(bindingResult.hasErrors())
            return "error";

        clientService.saveClientCommand(clientCommand);
        return "redirect:/clients";
    }

    @RequestMapping({"/clients/{id}/delete"})
    public String deleteClient(@PathVariable String id){
        clientService.delete(Long.valueOf(id));
        return "redirect:/clients";
    }

}
