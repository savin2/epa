package com.automotive.controllers;

import com.automotive.model.PurchaseOrder;
import com.automotive.services.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.EntityManager;

@Controller
public class IndexController {

    private final ClientService clientService;

    public IndexController(ClientService clientService){
        this.clientService = clientService;
    }

    @RequestMapping({"", "/", "/index"})
    public String getIndexPage(){
        clientService.createContext();
        return "index";
    }
    @GetMapping("/showLogInForm")
    public String showLogInForm(){
        return "login";
    }

    @GetMapping("/showErrorLogIn")
    public String showErrorLogIn(Model model){
        model.addAttribute("errorMessage", "try again ... ");
        return "login";
    }

    @GetMapping("/logout")
    public String logout(){
        return "login";
    }

    @GetMapping("/accessDenied")
    public String accessDenied() {
        return "access-denied";
    }

    }
