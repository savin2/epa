package com.automotive.controllers;

import com.automotive.commands.PurchaseOrderCommand;
import com.automotive.model.PurchaseOrder;
import com.automotive.services.ClientService;
import com.automotive.services.FactoryService;
import com.automotive.services.PurchaseOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class PurchaseOrderController {

    private final PurchaseOrderService purchaseOrderService;
    private final FactoryService factoryService;
    private final ClientService clientService;

    public PurchaseOrderController(PurchaseOrderService purchaseOrderService, FactoryService factoryService, ClientService clientService){
        this.purchaseOrderService = purchaseOrderService;
        this.factoryService = factoryService;
        this.clientService = clientService;
    }

    @RequestMapping({"/orders"})
    public String getPurchaseOrders(Model model){
        model.addAttribute("orders", purchaseOrderService.getPurchaseOrders());
        return "orders";
    }

    @RequestMapping("orders/new")
    public String newPurchaseOrder(Model model){
        model.addAttribute("order", new PurchaseOrderCommand());
        model.addAttribute("factories", factoryService.getFactories());
        model.addAttribute("clients", clientService.getClients());
        return "orderForm";
    }

    @RequestMapping("orders/{id}/edit")
    public String editPurchaseOrder(@PathVariable String id, Model model){
        model.addAttribute("order", purchaseOrderService.getPurchaseOrderCommand(Long.valueOf(id)));
        model.addAttribute("factories", factoryService.getFactories());
        model.addAttribute("clients", clientService.getClients());
        return "orderForm";
    }

    @PostMapping("orders")
    public String saveOrder(@Valid @ModelAttribute PurchaseOrderCommand order, BindingResult bindingResult){

        if(bindingResult.hasErrors())
            return "error";

        purchaseOrderService.savePurchaseOrderCommand(order);
        return "redirect:/orders";
    }

    @GetMapping("orders/{id}/delete")
    public String deleteOrder(@PathVariable String id)
    {
        purchaseOrderService.deletePurchaseOrder(Long.valueOf(id));
        return "redirect:/orders";
    }

    @RequestMapping("orders/{id}/details")
    public String getOrderDetails(@PathVariable String id, Model model){
        model.addAttribute("order", purchaseOrderService.getPurchaseOrder(Long.valueOf(id)));
        return "orderDetails";
    }


}
