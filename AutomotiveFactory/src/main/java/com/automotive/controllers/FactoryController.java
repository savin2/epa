package com.automotive.controllers;

import com.automotive.commands.FactoryCommand;
import com.automotive.services.CarModelService;
import com.automotive.services.FactoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AllArgsConstructor
@Controller
public class FactoryController {

    private final FactoryService factoryService;
    private final CarModelService carModelService;

    @RequestMapping({"/factories"})
    public String getFactories(Model model){
        model.addAttribute("factories", factoryService.getFactories());
        return "factories";
    }

    @RequestMapping("/factories/new")
    public String addFactory(Model model){
        model.addAttribute("factory", new FactoryCommand());
        model.addAttribute("carModels", carModelService.getCarModels());
        return "factoryForm";
    }

    @RequestMapping("factories/{id}/edit")
    public String editFactory(@PathVariable String id, Model model){
        model.addAttribute("factory", factoryService.getFactoryCommand(Long.valueOf(id)));
        model.addAttribute("carModels", carModelService.getCarModels());
        return "factoryForm";
    }

    @PostMapping("factories")
    public String saveFactory(@Valid @ModelAttribute FactoryCommand factory, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "error";
        }

        factoryService.saveFactoryCommand(factory);
        return "redirect:/factories";
    }


    @GetMapping("factories/{id}/delete")
    public String deleteFactory(@PathVariable String id){
        factoryService.delete(Long.valueOf(id));
        return "redirect:/factories";
    }

    @GetMapping("factories/{id}/deleteCarModel/{carModelId}")
    public String deleteCarModel(@PathVariable String id, @PathVariable String carModelId){
        factoryService.deleteCarModel(Long.valueOf(id), Long.valueOf(carModelId));
        return "redirect:/factories/" + id + "/details";
    }

    @RequestMapping("factories/{id}/details")
    public String getFactoryDetails(@PathVariable String id, Model model){
        model.addAttribute("factory",factoryService.getFactoryCommand(Long.valueOf(id)));
        return "factoryDetails";
    }

}
