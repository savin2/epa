package com.automotive.controllers;

import com.automotive.commands.CarModelCommand;
import com.automotive.model.CarModel;
import com.automotive.services.CarModelService;
import com.automotive.services.CarService;
import com.automotive.services.FactoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
public class CarModelController {

    private final CarModelService carModelService;
    private final FactoryService factoryService;

    public CarModelController(CarModelService carModelService, FactoryService factoryService){
        this.carModelService = carModelService;
        this.factoryService = factoryService;
    }


    @RequestMapping({"/carModels"})
    public String getCarModels(Model model){
        model.addAttribute("carModels", carModelService.getCarModels());
        return "carModels";
    }

    @RequestMapping("carModels/{id}/edit")
    public String editCarModel(@PathVariable String id, Model model){
        model.addAttribute("carModel", carModelService.getCarModelCommand(Long.valueOf(id)));
        model.addAttribute("factories", factoryService.getFactories());
        return "carModelForm";
    }

    @RequestMapping("carModels/{id}/details")
    public String carModelDetails(@PathVariable String id, Model model){
        model.addAttribute("carModel", carModelService.getCarModel(Long.valueOf(id)));
        return "carModelDetails";
    }

    @RequestMapping("/carModels/new")
    public String newCarModel(Model model){
        model.addAttribute("carModel", new CarModelCommand());
        model.addAttribute("factories", factoryService.getFactories());
        return "carModelForm";
    }

    @PostMapping("carModels")
    public String saveOrUpdate(@Valid @ModelAttribute CarModelCommand carModel, BindingResult bindingResult){

        if(bindingResult.hasErrors())
            return "error";

        carModelService.saveCarModelCommand(carModel);
        return "redirect:/carModels";
    }

    @RequestMapping("/carModels/{id}/delete")
    public String deleteCarModel(@PathVariable Long id){
        carModelService.deleteCarModel(Long.valueOf(id));
        return "redirect:/carModels";
    }

}
