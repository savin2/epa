package com.automotive.controllers;

import com.automotive.commands.CarCommand;
import com.automotive.model.Car;
import com.automotive.model.CarModel;
import com.automotive.model.PurchaseOrder;
import com.automotive.services.CarModelService;
import com.automotive.services.CarService;
import com.automotive.services.PurchaseOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class CarController {

    private final CarService carService;
    private final CarModelService carModelService;
    private final PurchaseOrderService purchaseOrderService;

    public CarController(CarService carService, CarModelService carModelService, PurchaseOrderService purchaseOrderService) {
        this.carService = carService;
        this.carModelService = carModelService;
        this.purchaseOrderService = purchaseOrderService;
    }


    @RequestMapping({"/cars"})
    public String getCars(Model model){
        model.addAttribute("cars", carService.getCars());
        return "cars";
    }

    @GetMapping("/cars/{orderId}/delete/{id}")
    public String deleteCar(@PathVariable String orderId, @PathVariable String id){
        carService.deleteCar(Long.valueOf(id));
        return "redirect:/orders/" + orderId + "/details";
    }

    @RequestMapping("orders/{orderId}/details/newCar")
    public String addCar(@PathVariable String orderId, Model model){
        CarCommand carCommand = new CarCommand();
        carCommand.setPurchaseOrder(purchaseOrderService.getPurchaseOrder(Long.valueOf(orderId)));
        model.addAttribute("car", carCommand);
        model.addAttribute("carModels", carModelService.getCarModels());
        return "carForm";
    }

    @RequestMapping("orders/{orderId}/details/cars/{id}")
    public String editCar(@PathVariable String orderId, @PathVariable String id, Model model){
        model.addAttribute("car", carService.getCarCommand(Long.valueOf(id)));
        model.addAttribute("carModels", carModelService.getCarModels());
        return "carForm";
    }

    @PostMapping("orders/{orderId}/details")
    public String saveCar(@Valid @ModelAttribute CarCommand carCommand, BindingResult bindingResult){

        if(bindingResult.hasErrors())
            return "error";

        carService.saveCarCommand(carCommand);
        return "redirect:/orders/" + carCommand.getPurchaseOrder().getId() + "/details";
    }

}
