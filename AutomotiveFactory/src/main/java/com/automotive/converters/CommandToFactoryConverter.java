package com.automotive.converters;

import com.automotive.commands.FactoryCommand;
import com.automotive.model.Factory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CommandToFactoryConverter implements Converter<FactoryCommand, Factory> {

    @Nullable
    @Override
    public Factory convert(FactoryCommand factoryCommand){
        if(factoryCommand == null)
            return null;

        final Factory factory = new Factory();
        factory.setId(factoryCommand.getId());
        factory.setName(factoryCommand.getName());
        factory.setCountry(factoryCommand.getCountry());
        factory.setCarModels(factoryCommand.getCarModels());
        factory.setPurchaseOrders(factoryCommand.getPurchaseOrders());

        return factory;
    }

}
