package com.automotive.converters;

import com.automotive.commands.FactoryCommand;
import com.automotive.commands.PartCommand;
import com.automotive.model.Factory;
import com.automotive.model.Part;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class PartToCommandConverter implements Converter<Part, PartCommand> {

    @Nullable
    @Override
    public PartCommand convert(Part part){
        if(part == null)
            return null;


        final PartCommand partCommand = new PartCommand();
        partCommand.setId(part.getId());
        partCommand.setName(part.getName());
        partCommand.setQuantity(part.getQuantity());
        partCommand.setDescription(part.getDescription());
        partCommand.setFactory(part.getFactory());
        partCommand.setSupplierIds(part.getSuppliers().stream().map(s->s.getId()).collect(Collectors.toSet()));
        partCommand.setSuppliers(part.getSuppliers());

        return partCommand;
    }

}