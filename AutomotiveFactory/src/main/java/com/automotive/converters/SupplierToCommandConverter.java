package com.automotive.converters;

import com.automotive.commands.SupplierCommand;
import com.automotive.model.Supplier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class SupplierToCommandConverter implements Converter<Supplier, SupplierCommand> {

    public SupplierCommand convert(Supplier supplier){

        if(supplier == null)
            return null;

        SupplierCommand s = new SupplierCommand();
        s.setId(supplier.getId());
        s.setName(supplier.getName());
        s.setAddress((supplier.getAddress()));
        s.setEmail((supplier.getEmail()));
        s.setPartIds(supplier.getParts().stream().map(p -> p.getId()).collect(Collectors.toSet()));
        return s;

    }

}
