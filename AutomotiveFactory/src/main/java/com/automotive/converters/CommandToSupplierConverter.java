package com.automotive.converters;

import com.automotive.commands.SupplierCommand;
import com.automotive.model.Supplier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommandToSupplierConverter implements Converter<SupplierCommand, Supplier> {

    public Supplier convert(SupplierCommand supplierCommand){
        if(supplierCommand == null)
            return null;

        Supplier supplier = new Supplier();
        supplier.setId(supplierCommand.getId());
        supplier.setName(supplierCommand.getName());
        supplier.setAddress(supplierCommand.getAddress());
        supplier.setEmail(supplierCommand.getEmail());
        return supplier;

    }

}
