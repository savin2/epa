package com.automotive.converters;

import com.automotive.commands.PartCommand;
import com.automotive.model.Part;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CommandToPartConverter implements Converter<PartCommand, Part> {

    @Nullable
    @Override
    public Part convert(PartCommand partCommand){

        if(partCommand == null)
            return null;

        final Part p = new Part();
        p.setId(partCommand.getId());
        p.setName(partCommand.getName());
        p.setDescription(partCommand.getDescription());
        p.setQuantity(partCommand.getQuantity());
        p.setFactory(partCommand.getFactory());

        return p;

    }

}
