package com.automotive.converters;

import com.automotive.commands.FactoryCommand;
import com.automotive.model.Factory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component

public class FactoryToCommandConverter implements Converter<Factory, FactoryCommand> {

    @Nullable
    @Override
    public FactoryCommand convert(Factory factory){
        if(factory == null)
            return null;

        final FactoryCommand factoryCommand = new FactoryCommand();
        factoryCommand.setId(factory.getId());
        factoryCommand.setName(factory.getName());
        factoryCommand.setCountry(factory.getCountry());
        factoryCommand.setSelectedCarModels(factory.getCarModels().stream().map(c -> c.getId()).collect(Collectors.toList()));
        factoryCommand.setPurchaseOrders(factory.getPurchaseOrders());
        factoryCommand.setCarModels(factory.getCarModels());
        return factoryCommand;
    }

}
