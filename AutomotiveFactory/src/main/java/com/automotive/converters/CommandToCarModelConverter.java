package com.automotive.converters;

import com.automotive.commands.CarModelCommand;
import com.automotive.model.CarModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CommandToCarModelConverter implements Converter<CarModelCommand, CarModel> {

    @Nullable
    @Override
    public CarModel convert(CarModelCommand carModelCommand){
        if(carModelCommand == null)
            return null;

        final CarModel carModel = new CarModel();

        carModel.setId(carModelCommand.getId());
        carModel.setName(carModelCommand.getName());
        carModel.setBasePrice(carModelCommand.getBasePrice());
        carModel.setImage(carModelCommand.getImage());
        carModel.setType(carModelCommand.getType());
        carModel.setCars(carModelCommand.getCars());
        carModel.setFactories(carModelCommand.getFactories());

        return carModel;
    }

}
