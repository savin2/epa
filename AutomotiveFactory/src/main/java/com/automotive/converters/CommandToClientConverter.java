package com.automotive.converters;

import com.automotive.commands.ClientCommand;
import com.automotive.model.Client;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CommandToClientConverter implements Converter<ClientCommand, Client> {

    @Nullable
    @Override
    public Client convert(ClientCommand clientCommand){
        if(clientCommand == null)
            return null;

        Client client = new Client();
        client.setId(clientCommand.getId());
        client.setName(clientCommand.getName());
        client.setBirthdate((clientCommand.getBirthdate()));
        client.setCNP((clientCommand.getCNP()));
        client.setPurchaseOrders(clientCommand.getPurchaseOrders());

        return client;
    }

}
