package com.automotive.converters;

import com.automotive.commands.PurchaseOrderCommand;
import com.automotive.model.PurchaseOrder;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class PurchaseOrderToCommandConverter implements Converter<PurchaseOrder, PurchaseOrderCommand> {

    @Nullable
    @Override
    public PurchaseOrderCommand convert(PurchaseOrder purchaseOrder)
    {
        if(purchaseOrder == null)
            return null;

        PurchaseOrderCommand purchaseOrderCommand = new PurchaseOrderCommand();
        purchaseOrderCommand.setId(purchaseOrder.getId());
        purchaseOrderCommand.setCode(purchaseOrder.getCode());
        purchaseOrderCommand.setCost(purchaseOrder.getCost());
        purchaseOrderCommand.setCompleted(purchaseOrder.isCompleted());
        purchaseOrderCommand.setRequestDate(purchaseOrder.getRequestDate());
        purchaseOrderCommand.setFactory(purchaseOrder.getFactory());
        purchaseOrderCommand.setClient(purchaseOrder.getClient());
        return purchaseOrderCommand;
    }

}
